package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public interface UserService {

    List<User> listAll();

    User fetch(long id);

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByOib(String oib);

    User createUser(User user);

    User createUser(User user, int roleId);

    User createMechanic(String companyName, User user);

    User updateUser(User user);

    User updateUserPassword(User user, String password);

    Optional<User> getUser(String username);

 /*   Set<Vehicle> findAll(String username);*/

    void deleteByUsername(String username);

    boolean checkPassword(String plainPassword, String encryptedPassword);
}
