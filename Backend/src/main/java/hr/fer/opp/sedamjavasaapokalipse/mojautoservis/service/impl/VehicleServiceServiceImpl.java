package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.*;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.VehicleService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration.OrderType;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration.ServiceType;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.VehicleServiceRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VehicleServiceServiceImpl implements VehicleServiceService {

    private final VehicleServiceRepository vehicleServiceRepo;

    private final AutoservisService autoServisService;

    private final VehicleServiceImpl vehicleServiceImpl;

    private final UserService userService;

    public VehicleServiceServiceImpl(VehicleServiceRepository vehicleServiceRepo, @Qualifier("autoservisServiceImpl") AutoservisService autoServisService, VehicleServiceImpl vehicleServiceImpl, UserService userService) {
        this.vehicleServiceRepo = vehicleServiceRepo;
        this.autoServisService = autoServisService;
        this.vehicleServiceImpl = vehicleServiceImpl;
        this.userService = userService;
    }

    @Override
    public List<VehicleService> listAll() {
        return vehicleServiceRepo.findAll();
    }

    @Override
    public List<VehicleService> listAllByMechanicAndOpenOrder(String mechanicName) {

        Assert.notNull(mechanicName, "Mechanic name must not be null");

        User mechanic = userService.findByUsername(mechanicName).orElseThrow(() -> new MissingEntityException(User.class, mechanicName));
        Autoservis autoservis = mechanic.getAutoservis();
        if(autoservis == null)
            throw new RequestDeniedException("Mechanic does not have a company");

        return autoservis.getVehicleServices().stream()
                .filter(vs -> vs.getOrder().getOrderType() == OrderType.OPEN)
                .collect(Collectors.toList());
    }

    @Override
    public List<VehicleService> listAllByUserAndOpenOrder(String username) {
        Assert.notNull(username, "Username must not be null");

        User user = userService.findByUsername(username).orElseThrow(() -> new MissingEntityException(User.class, username));
        Set<Vehicle> vehicles = user.getVehicles();
        return vehicles.stream()
                .filter(vehicleServiceRepo::existsByVehicle)
                .map(vehicleServiceRepo::findByVehicle)
                .collect(Collectors.toList());
    }

    @Override
    public VehicleService fetch(long serviceId) {
        return vehicleServiceRepo.findById(serviceId).orElseThrow(() -> new MissingEntityException(VehicleService.class, serviceId));
    }

    @Override
    public VehicleService createVehicleService(VehicleService vehicleService) {

        validate(vehicleService);

        if(autoServisService.findByCompanyName(vehicleService.getAutoservis().getCompanyName()).isEmpty())
            throw new RequestDeniedException("Company does not exist!");

        if(vehicleServiceImpl.findByRegistrationMark(vehicleService.getVehicle().getRegistrationMark()).isEmpty())
            throw new RequestDeniedException("Vehicle does not exist!");

        if(vehicleServiceRepo.existsByVehicle(vehicleService.getVehicle()))
            throw new RequestDeniedException("This vehicle already is on service");

        vehicleService.setTimeStarted(LocalDateTime.now());
        vehicleService.setServiceType(ServiceType.REGULAR);
        Order order = new Order(OrderType.OPEN);
        order.setVehicleService(vehicleService);
        vehicleService.setOrder(order);
        return vehicleServiceRepo.save(vehicleService);
    }

    @Override
    public VehicleService updateVehicleService(VehicleService vehicleService) {
        validate(vehicleService);
        Long serviceId = vehicleService.getServiceId();

        if(!vehicleServiceRepo.existsByServiceId(serviceId))
            throw new MissingEntityException(VehicleService.class, serviceId);

        vehicleService.setAutoservis(fetch(serviceId).getAutoservis());
        return vehicleServiceRepo.save(vehicleService);
    }

    private void validate(VehicleService vehicleService) {
        Assert.notNull(vehicleService, "Vehicle service must not be null");
        Assert.notNull(vehicleService.getVehicle(), "Vehicle must not be null");
    }
}
