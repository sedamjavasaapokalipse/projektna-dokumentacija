package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/parts")
public class PartController {

    private final PartService partService;

    @Autowired
    public PartController(PartService partService) {
        this.partService = partService;
    }

    @GetMapping("/by-company-name/{companyName}")
    public List<Part> retrieveAllByCompanyName(@PathVariable String companyName) {
        return partService.listAllByCompanyName(companyName);
    }

    @GetMapping("/by-mechanic/{mechanicUsername}")
    public List<Part> retrieveAllByMechanic(@PathVariable String mechanicUsername) {
        return partService.listAllByMechanicUsername(mechanicUsername);
    }

    @GetMapping("/{id}")
    public Part fetch(@PathVariable long id) {
        return partService.fetch(id);
    }

    @PostMapping("/create/{companyName}")
    public ResponseEntity<Part> createPart(@PathVariable String companyName, @RequestBody Part part) {
        Part created = partService.createPart(companyName, part);
        return new ResponseEntity<>(created, HttpStatus.OK);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Part> updatePart(@PathVariable Long id, @RequestBody Part part) {
        Part updated = partService.updatePart(id, part);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Part> deletePart(@PathVariable Long id) {
        Part deleted = partService.deletePart(id);
        return new ResponseEntity<>(deleted, HttpStatus.OK);
    }
}
