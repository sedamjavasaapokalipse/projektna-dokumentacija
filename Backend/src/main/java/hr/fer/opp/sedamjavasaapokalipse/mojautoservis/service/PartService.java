package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PartService {
    List<Part> listAllByCompanyName(String autoservis);

    List<Part> listAllByMechanicUsername(String mechanicUsername);

    Part fetch(Long id);

    Part createPart(String companyName, Part part);

    Part updatePart(Long id, Part newPart);

    Part deletePart(Long id);

    Optional<Part> findByPartName(String partName);
}
