package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@ToString(exclude = {"user", "vehicleServices"})
@EqualsAndHashCode(exclude = {"user", "vehicleServices"})
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min=6, max=8)
    private String registrationMark;

    @NotNull
    private String brand;

    @NotNull
    private String model;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference(value = "user-vehicles")
    private User user;

    @OneToMany(mappedBy = "vehicle", fetch=FetchType.LAZY, cascade= CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<VehicleService> vehicleServices;

}
