package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface RoleRepository extends JpaRepository<Role, Long> {
}
