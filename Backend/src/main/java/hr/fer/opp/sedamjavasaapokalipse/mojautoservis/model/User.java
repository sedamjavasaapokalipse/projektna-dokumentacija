package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
//needed because user is an sql reserved keyword
@Table(name = "users")
@Data
//solves lombok recursion
@ToString(exclude = {"roles", "autoservis", "vehicles", "reviews"})
@EqualsAndHashCode(exclude = {"roles", "autoservis", "vehicles", "reviews"})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    @Size(min = 6)
    private String password;

    @Column(unique = true, nullable = false)
    @NotNull
    @Size(min = 11, max = 11)
    private String oib;

    @NotNull
    @Email
    @Column(unique = true)
    private String email;

    @NotNull
    @Column(unique = true)
    private String username;

    @ManyToOne
    @JoinColumn(name = "autoservis_id")
    @JsonBackReference(value = "user-autoservis")
    private Autoservis autoservis;

    @NotNull
    @ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE})
    @JoinTable(name = "user_role", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "role_id", nullable = false)})
    @JsonIgnore
    private Set<Role> roles;

    @OneToMany(mappedBy = "user", fetch=FetchType.LAZY, cascade= CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value = "user-vehicles")
    @JsonIgnore
    private Set<Vehicle> vehicles;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value="user-reviews")
    private List<Review> reviews;
}
