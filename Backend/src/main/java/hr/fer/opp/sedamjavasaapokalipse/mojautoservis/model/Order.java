package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration.OrderType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "orders")
@Data
@ToString(exclude = "vehicleService")
@EqualsAndHashCode(exclude = "vehicleService")
public class Order {

    @Id
    private Long serviceId;

    @Enumerated(EnumType.STRING)
    OrderType orderType;

    @OneToOne(cascade=CascadeType.ALL)
    @MapsId
    @JoinColumn(name="serviceId")
    @JsonBackReference
    private VehicleService vehicleService;

    public Order() {
    }

    public Order(OrderType orderType) {
        this.orderType = orderType;
    }
}
