package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Huo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HuoRepository extends JpaRepository<Huo, String> {

    boolean existsByRegistrationMark(String registrationMark);
}
