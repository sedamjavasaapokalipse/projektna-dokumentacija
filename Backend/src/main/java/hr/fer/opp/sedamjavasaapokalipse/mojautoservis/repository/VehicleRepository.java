package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

/*    Set<Vehicle> findAll(@Param("s") String username);*/

    int countByRegistrationMark(String registrationMark);

    Optional<Vehicle> findByRegistrationMark(String registrationMark);

    void deleteByRegistrationMark(String registrationMark);
}
