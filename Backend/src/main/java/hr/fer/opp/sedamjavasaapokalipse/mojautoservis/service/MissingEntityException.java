package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class MissingEntityException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MissingEntityException(Class<?> cls, Object ref) {
        super("Entity with reference: " + ref + " of " + cls + " not found.");
    }

}
