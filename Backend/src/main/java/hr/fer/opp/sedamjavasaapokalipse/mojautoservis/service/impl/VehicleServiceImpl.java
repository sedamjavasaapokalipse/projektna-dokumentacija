package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.UserRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.VehicleRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.MissingEntityException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.RequestDeniedException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.UserService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final UserService userService;
    private final UserRepository userRepo;
    private static final String REGISTRATION_MARK_FORMAT = "^(BJ|BM|ČK|DA|DE|DJ|DU|GS|IM|KA|KC|KR|KT|KŽ|" +
            "MA|NA|NG|OG|OS|PU|PŽ|RI|SB|SK|SL|PS|ST|ŠI|VK|VT|VU|VŽ|ZD|ZG|ŽU)\\d{3,4}\\p{L}{1,2}$";

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository, UserService userService, UserRepository userRepo) {
        this.vehicleRepository = vehicleRepository;
        this.userService = userService;
        this.userRepo = userRepo;
    }

    @Override
    public Set<Vehicle> listAll(String username) {
        User user = userRepo.findByUsername(username).orElseThrow();
        System.out.println(user.getVehicles() + "OVDJEEEEEEEEEEEEEEEEEEE");
        /*        return vehicleRepository.findAll(username);*/
        return user.getVehicles() == null ? new LinkedHashSet<>() : user.getVehicles();
    }

    @Override
    public Vehicle fetch(String registrationMark) {
        return findByRegistrationMark(registrationMark).orElseThrow(() -> new MissingEntityException(Vehicle.class, registrationMark));
    }

    @Override
    public Optional<Vehicle> findByRegistrationMark(String registrationMark) {
        Assert.notNull(registrationMark, "Registration mark must be given");
        return vehicleRepository.findByRegistrationMark(registrationMark);
    }

    @Override
    public Vehicle createVehicle(Vehicle vehicle, String username) {
        vehicle.setRegistrationMark(vehicle.getRegistrationMark().toUpperCase());
        validateVehicle(vehicle);
        if (vehicleRepository.countByRegistrationMark(vehicle.getRegistrationMark()) > 0) {
            throw new RequestDeniedException("Vehicle with " + vehicle.getRegistrationMark() + " already exists");
        }

        System.out.println("Dodajem...");
        User user = userService.getUser(username).orElseThrow(() ->
                new RequestDeniedException("No user with username " + username));

        Set<Vehicle> vehicles = user.getVehicles() == null ? new LinkedHashSet<>() : user.getVehicles();
        vehicles.add(vehicle);
        user.setVehicles(vehicles);
        vehicle.setUser(user);

        Vehicle saved = vehicleRepository.save(vehicle);

        userRepo.save(user);

        System.out.println("Dodao, nova marka: " + vehicle.getBrand());
        return saved;
    }

    @Override
    public Vehicle updateVehicle(Long id, Vehicle newVehicle) {
        validateVehicle(newVehicle);
        Assert.notNull(id, "Id must be given");
        Optional<Vehicle> optionalVehicle= vehicleRepository.findById(id);
        Assert.isTrue(optionalVehicle.isPresent(), "Vehicle does not exist");
        Vehicle oldVehicle = optionalVehicle.get();

        newVehicle.setUser(oldVehicle.getUser());

        vehicleRepository.save(newVehicle);
        return newVehicle;
    }

    @Override
    public Optional<Vehicle> getVehicle(String registrationMark) {
        return Optional.empty();
    }

    @Override
    public void deleteByRegistrationMark(String registrationMark) {
        vehicleRepository.deleteByRegistrationMark(registrationMark);
    }

    private void validateVehicle(Vehicle vehicle) {
        Assert.notNull(vehicle, "Vehicle object must be given!");
        String regMark = vehicle.getRegistrationMark();
        Assert.hasText(regMark, "Registration mark must be given");
        Assert.isTrue(regMark.matches(REGISTRATION_MARK_FORMAT), "Registration mark must be of type XX0000-XX");
    }
}
