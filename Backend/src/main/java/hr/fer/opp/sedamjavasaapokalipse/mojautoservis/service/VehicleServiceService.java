package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.VehicleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface VehicleServiceService {

    List<VehicleService> listAll();

    List<VehicleService> listAllByMechanicAndOpenOrder(String mechanicName);

    List<VehicleService> listAllByUserAndOpenOrder(String username);

    VehicleService fetch(long serviceId);

    VehicleService createVehicleService(VehicleService vehicleService);

    VehicleService updateVehicleService(VehicleService vehicleService);
}
