package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Location;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.RoleService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.transaction.Transactional;
import java.net.URI;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/autoservisi")
public class AutoservisController {

    private static class UserAutoservisHolder {
        private Autoservis autoservis;
        private User user;

        public UserAutoservisHolder(Autoservis autoservis, User user) {
            this.autoservis = autoservis;
            this.user = user;
        }
    }

    private final AutoservisService autoServisService;

    private final UserService userService;

    public AutoservisController(@Qualifier("autoservisServiceImpl") AutoservisService autoServisService,
                                UserService userService) {
        this.autoServisService = autoServisService;
        this.userService = userService;
    }

    @GetMapping("")
    public List<Autoservis> retrieveAutoservices() {
        return autoServisService.listAll();
    }

    @GetMapping("/by_id/{id}")
    public Autoservis retrieveAutoservis(@PathVariable long id) {
        return autoServisService.fetch(id);
    }

    @GetMapping("/{companyName}")
    public Autoservis getAutoservis(@PathVariable String companyName) {
        return autoServisService.findByCompanyName(companyName).orElseThrow();
    }

    @PostMapping("")
    public ResponseEntity<Autoservis> createAutoservis(@RequestBody UserAutoservisHolder userAutoservisHolder) {
        Autoservis createdAutoservis = autoServisService.createAutoservis(userAutoservisHolder.autoservis);
        userAutoservisHolder.user.setAutoservis(createdAutoservis);
        User createdUser = userService.createUser(userAutoservisHolder.user, Role.ADMIN);
        return ResponseEntity.created(URI.create("/" + createdAutoservis.getId())).body(createdAutoservis);
    }

    @PatchMapping("")
    public ResponseEntity<Autoservis> updateAutoservis(@RequestBody Autoservis autoservis) {
        Autoservis oldAutoservis = autoServisService.fetch(autoservis.getId());
        autoservis.setParts(oldAutoservis.getParts());
        autoservis.setUsers(oldAutoservis.getUsers());
        Autoservis updatedAutoservis = autoServisService.updateAutoservis(autoservis);
        return new ResponseEntity<>(updatedAutoservis, HttpStatus.OK);
    }

    @GetMapping("/mechanics/{companyName}")
    public ResponseEntity<List<User>> listAllMechanics(@PathVariable String companyName) {
        return new ResponseEntity<>(autoServisService.listAllMechanics(companyName), HttpStatus.OK);
    }

    @GetMapping("/avalilableLocation")
    public Boolean isLocationAvailable(@RequestBody Location location) {
        return autoServisService.findByLocation(location).isEmpty();
    }
}

