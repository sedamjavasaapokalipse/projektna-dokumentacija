package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity
@Data
@ToString(exclude = {"users", "reviews", "parts", "vehicleServices"})
@EqualsAndHashCode(exclude = {"users", "reviews", "parts", "vehicleServices"})
public class Autoservis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String companyName;

    @Column(unique = true, nullable = false)
    @NotNull
    @Size(min = 11, max = 11)
    private String oib;

    @NotNull
    @Email
    @Column(unique = true)
    private String email;

//    @OneToOne(mappedBy = "autoservis", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "location_id", referencedColumnName = "id")
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(mappedBy = "autoservis", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value="autoservis-reviews")
    private List<Review> reviews;

    @OneToMany(mappedBy = "autoservis", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @JsonIgnore
    private List<User> users;

    @OneToMany(mappedBy = "autoservis", fetch=FetchType.LAZY, cascade= CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value = "autoservis-vehicleService")
    private List<VehicleService> vehicleServices;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE})
    @JoinTable(name = "autoservis_part", joinColumns = {
            @JoinColumn(name = "autoservis_id", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "part_id", nullable = false)})
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Part> parts;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "autoservis_service", joinColumns = {
            @JoinColumn(name = "autoservis_id", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "service_id", nullable = false)})
    @JsonIgnore
    private Set<Service> services;

}
