package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {

    List<Role> listAll();

    Role fetch(long id);

}
