package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.security;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    public JwtUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException("Username does not exist in database"));

        return new JwtUserDetails(user.getUsername(), user.getPassword(), user.getRoles().stream()
                .map(Role::getRoleName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList()));
    }

}
