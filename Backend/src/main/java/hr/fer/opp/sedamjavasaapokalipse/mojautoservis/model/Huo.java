package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Huo {

    @Id
    private String registrationMark;
}
