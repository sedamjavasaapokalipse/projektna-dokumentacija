package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.HuoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/huo")
public class HuoController {

    final
    HuoService huoService;

    public HuoController(HuoService huoService) {
        this.huoService = huoService;
    }

    @GetMapping("/{registrationMark}")
    public ResponseEntity<Boolean> existsByRegistrationMark(@PathVariable String registrationMark) {
        return new ResponseEntity<>(huoService.existsByRegistrationMark(registrationMark), HttpStatus.OK);
    }
}
