package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AutoservisRepository extends JpaRepository<Autoservis, Long> {

    Optional<Autoservis> findByCompanyName(String username);

    Optional<Autoservis> findByEmail(String email);

    Optional<Autoservis> findByOib(String oib);

    int countByOib(String oib);

    boolean existsByOibAndIdNot(String oib, Long id);

    boolean existsByEmailAndIdNot(String email, Long id);

    boolean existsByCompanyNameAndIdNot(String username, Long id);
}
