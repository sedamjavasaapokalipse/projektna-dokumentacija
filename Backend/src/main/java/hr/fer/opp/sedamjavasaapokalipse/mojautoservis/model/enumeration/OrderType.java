package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration;

public enum OrderType {
    CLOSED,
    OPEN
}
