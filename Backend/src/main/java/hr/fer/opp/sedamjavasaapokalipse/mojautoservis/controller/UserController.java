package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.net.URI;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final AutoservisService autoservisService;

    @Autowired
    public UserController(UserService userService, AutoservisService autoservisService) {
        this.userService = userService;
        this.autoservisService = autoservisService;
    }

    @PostMapping("")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        User created = userService.createUser(user);
        return ResponseEntity.created(URI.create("/" + created.getId())).body(created);
    }

    @PostMapping("/{companyName}")
    public ResponseEntity<User> createMechanic(@PathVariable String companyName, @RequestBody User user) {
        User created = userService.createMechanic(companyName, user);
        return ResponseEntity.created(URI.create("/" + created.getId())).body(created);
    }

    @Transactional
    @PatchMapping("/remove/{username}")
    public ResponseEntity<User> removeMechanic(@PathVariable String username) {
        User user = userService.findByUsername(username).orElseThrow();
        userService.deleteByUsername(username);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Transactional
    @PatchMapping("")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        User oldUser = userService.fetch(user.getId());

        user.setAutoservis(oldUser.getAutoservis());
        user.setRoles(oldUser.getRoles());
        user.setVehicles(oldUser.getVehicles());

        User updatedUser = userService.updateUser(user);

        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @Transactional
    @PatchMapping("/{id}")
    public ResponseEntity<User> updateUserPassword(@PathVariable Long id, @RequestBody User user) {
        User user1 = userService.fetch(id);
        User updatedUser = userService.updateUserPassword(user1, user.getPassword());

        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @Transactional
    @PatchMapping("/check/{username}")
    public ResponseEntity<User> checkPassword(@PathVariable String username, @RequestBody User user) {
        User user1 = userService.getUser(username).orElse(null);

        if (user1 == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!userService.checkPassword(user.getPassword(), user1.getPassword())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user1, HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username) {
        return userService.findByUsername(username)
                .map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Transactional
    @DeleteMapping("/{username}")
    public ResponseEntity<?> deleteGroup(@PathVariable String username) {
        userService.deleteByUsername(username);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/by_email")
    public ResponseEntity<User> getUserByEmail(@RequestParam("email") String email) {
        User user = userService.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("No such email"));
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/availableUsername")
    public ResponseEntity<Boolean> isUsernameAvailable(@RequestParam("username") String username) {
        return new ResponseEntity<>(userService.findByUsername(username).isEmpty() &&
                autoservisService.findByCompanyName(username).isEmpty(),
                HttpStatus.OK);
    }

    @GetMapping("/availableEmail")
    public ResponseEntity<Boolean> isEmailAvailable(@RequestParam("email") String email) {
        return new ResponseEntity<>(userService.findByEmail(email).isEmpty() &&
                autoservisService.findByEmail(email).isEmpty(),
                HttpStatus.OK);
    }

    @GetMapping("/availableOib")
    public ResponseEntity<Boolean> isOibAvailable(@RequestParam("oib") String oib) {
        return new ResponseEntity<>(userService.findByOib(oib).isEmpty() &&
                autoservisService.findByOib(oib).isEmpty(),
                HttpStatus.OK);
    }

}
