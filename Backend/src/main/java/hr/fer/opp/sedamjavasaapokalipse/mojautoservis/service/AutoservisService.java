package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;


import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Location;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AutoservisService {

    List<Autoservis> listAll();

    Autoservis fetch(long id);

    Optional<Autoservis> findByCompanyName(String companyName);

    Autoservis createAutoservis(Autoservis autoservis);

    Optional<Autoservis> findByEmail(String email);

    Optional<Autoservis> findByOib(String oib);

    Autoservis updateAutoservis(Autoservis autoservis);

    List<Autoservis> getAll();

    List<User> listAllMechanics(String companyName);

    Autoservis removeMechanic(String companyName, String username);

    Optional<Autoservis> findByMechanicUsername(String mechanicUsername);

    Optional<Autoservis> findByLocation(Location location);
}
