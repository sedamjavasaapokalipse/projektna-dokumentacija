package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PartRepository extends JpaRepository<Part, Long> {

    Optional<Part> findByPartName(String partName);
}
