package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.UserRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private static final String OIB_FORMAT = "[0-9]{11}";

    private final UserRepository userRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final AutoservisService autoservisService;

    public UserServiceImpl(UserRepository userRepo, PasswordEncoder passwordEncoder
            , RoleService roleService, AutoservisService autoservisService) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.autoservisService = autoservisService;
    }

    @Override
    public List<User> listAll() {
        return userRepo.findAll();
    }

    @Override
    public User fetch(long id) {
        return userRepo.findById(id).orElseThrow(() -> new MissingEntityException(User.class, id));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        Assert.notNull(username, "Username must be given");
        return userRepo.findByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        Assert.notNull(email, "Company name must be given");
        return userRepo.findByEmail(email);
    }

    @Override
    public Optional<User> findByOib(String oib) {
        Assert.notNull(oib, "Oib must be given");
        return userRepo.findByOib(oib);
    }

    @Override
    public User createUser(User user) {
        return createUser(user, Role.REGISTERED_USER);
    }

    @Override
    public User createUser(User user, int roleId) {
        userCreation(user, roleId);
        return userRepo.save(user);
    }

    @Override
    public User createMechanic(String companyName, User user){
        userCreation(user, Role.AUTO_MECHANIC);

        Assert.notNull(companyName, "Company name must be given");

        Optional<Autoservis> autoservisOpt = autoservisService.findByCompanyName(companyName);
        Assert.isTrue(autoservisOpt.isPresent(), "Autoservis " + companyName + " does not exist");

        Autoservis autoservis = autoservisOpt.get();

        List<User> users = autoservis.getUsers() == null ? new LinkedList<>() : autoservis.getUsers();
        users.add(user);
        user.setAutoservis(autoservis);

        return userRepo.save(user);
    }

    @Override
    public User updateUser(User user) {
        validate(user);
        Long userId = user.getId();

        if (!userRepo.existsById(userId))
            throw new MissingEntityException(User.class, userId);

        if (userRepo.existsByOibAndIdNot(user.getOib(), userId))
            throw new RequestDeniedException(
                    "User with OIB " + user.getOib() + " already exists");
        if (userRepo.existsByEmailAndIdNot(user.getEmail(), userId))
            throw new RequestDeniedException(
                    "User with email " + user.getEmail() + " already exists");
        if (userRepo.existsByUsernameAndIdNot(user.getUsername(), userId))
            throw new RequestDeniedException(
                    "User with username " + user.getUsername() + " already exists");

        User oldUser = userRepo.findById(user.getId()).orElseThrow();
        user.setPassword(oldUser.getPassword());
        user.setVehicles(oldUser.getVehicles());
        userRepo.save(user);

        return user;
    }

    @Override
    public User updateUserPassword(User user, String password) {
       user.setPassword(passwordEncoder.encode(password));
       userRepo.save(user);

       return user;
    }

    @Override
    public Optional<User> getUser(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public void deleteByUsername(String username) {
        userRepo.deleteByUsername(username);
    }

    @Override
    public boolean checkPassword(String plainPassword, String encryptedPassword) {
        return passwordEncoder.matches(plainPassword, encryptedPassword);
    }

    private void validate(User user) {
        Assert.notNull(user, "User must not be null");
        Assert.hasText(user.getUsername(), "Username must be given");
        Assert.hasText(user.getFirstName(), "First name must be given");
        Assert.hasText(user.getLastName(), "Last name must be given");
        Assert.hasText(user.getEmail(), "Email must be given");
        String oib = user.getOib();
        Assert.hasText(oib, "OIB must be given");
        Assert.isTrue(oib.matches(OIB_FORMAT),
                "OIB must have 11 digits. OIB entered has " + oib.length() + " digits.");
        Assert.isTrue(!user.getUsername().contains("@"), "Username contains '@' character");
    }

    private void userCreation(User user, int roleId) {
        validate(user);
        Assert.hasText(user.getPassword(), "Password must be given");

        Assert.isNull(user.getId(),
                "User ID should be null. Its value: " + user.getId());

        if (userRepo.countByOib(user.getOib()) > 0)
            throw new RequestDeniedException(
                    "User with OIB " + user.getOib() + " already exists");

//      temporary code, role use cases not yet defined (only login, register and home page exist)
        Set<Role> roles = new LinkedHashSet<>();
        roles.add(roleService.fetch(roleId));
        user.setRoles(roles);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
    }
}
