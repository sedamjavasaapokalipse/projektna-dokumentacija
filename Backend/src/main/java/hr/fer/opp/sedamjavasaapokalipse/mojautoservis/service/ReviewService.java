package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Review;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ReviewService {

    Review createReview(Review review, String username, String companyName);

    List<Review> listAll(String companyName);
}
