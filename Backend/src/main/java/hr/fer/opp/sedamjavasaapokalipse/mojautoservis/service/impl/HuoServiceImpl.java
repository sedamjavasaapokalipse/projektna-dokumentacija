package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.HuoRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.HuoService;
import org.springframework.stereotype.Service;

@Service
public class HuoServiceImpl implements HuoService {

    final
    HuoRepository huoRepository;

    public HuoServiceImpl(HuoRepository huoRepository) {
        this.huoRepository = huoRepository;
    }

    @Override
    public boolean existsByRegistrationMark(String registrationMark) {
        return huoRepository.existsByRegistrationMark(registrationMark);
    }
}
