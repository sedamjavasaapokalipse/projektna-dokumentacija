package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.VehicleService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleServiceRepository  extends JpaRepository<VehicleService, Long> {
    boolean existsByVehicle(Vehicle vehicle);
    boolean existsByServiceId(long serviceId);

    VehicleService findByVehicle(Vehicle vehicle);
}
