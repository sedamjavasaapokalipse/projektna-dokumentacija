package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.*;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.AutoservisRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.LocationRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.RoleRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.UserRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.MissingEntityException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.RequestDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AutoservisServiceImpl implements AutoservisService {

    private static final String OIB_FORMAT = "[0-9]{11}";

    private final AutoservisRepository autoservisRepo;

    private final LocationRepository locationRepository;

    private final RoleRepository roleRepo;

    private final UserRepository userRepo;

    public AutoservisServiceImpl(AutoservisRepository autoservisRepo
            , RoleRepository roleRepo, UserRepository userRepo, LocationRepository locationRepository) {
        this.autoservisRepo = autoservisRepo;
        this.roleRepo = roleRepo;
        this.userRepo = userRepo;
        this.locationRepository = locationRepository;
    }

    @Override
    public List<Autoservis> listAll() {
        return autoservisRepo.findAll();
    }

    @Override
    public Autoservis fetch(long id) {
        return autoservisRepo.findById(id).orElseThrow(() -> new MissingEntityException(Autoservis.class, id));
    }

    @Override
    public Optional<Autoservis> findByCompanyName(String companyName) {
        Assert.notNull(companyName, "Company name must be given");
        return autoservisRepo.findByCompanyName(companyName);
    }


    public Optional<Autoservis> findByEmail(String email) {
        Assert.notNull(email, "Company name must be given");
        return autoservisRepo.findByEmail(email);
    }

    @Override
    public Optional<Autoservis> findByOib(String oib) {
        Assert.notNull(oib, "Oib must be given");
        return autoservisRepo.findByOib(oib);
    }

    @Override
    public Optional<Autoservis> findByMechanicUsername(String mechanicUsername) {
        User mechanic = userRepo.findByUsername(mechanicUsername).orElseThrow();
        Autoservis company = mechanic.getAutoservis();
        Assert.notNull(company, "User is not mechanic");
        return autoservisRepo.findByCompanyName(company.getCompanyName());
    }

    @Override
    public Optional<Autoservis> findByLocation(Location location) {
        // TODO: 16.01.20. implement
        return Optional.empty();
    }

    @Override
    public Autoservis createAutoservis(Autoservis autoservis) {

        validate(autoservis);

        Assert.isNull(autoservis.getId(),
                "Autoservis ID should be null. Its value: " + autoservis.getId());

        Optional<Location> location = locationRepository.findFirstByPlace(autoservis.getLocation().getPlace());

        location.ifPresent(value -> autoservis.getLocation().setPlace(value.getPlace()));

        if (autoservisRepo.countByOib(autoservis.getOib()) > 0)
            throw new RequestDeniedException(
                    "Autoservis with OIB " + autoservis.getOib() + " already exists");

        return autoservisRepo.save(autoservis);
    }

    @Override
    public Autoservis updateAutoservis(Autoservis autoservis) {
        validate(autoservis);
        Long autoservisId = autoservis.getId();

        if (!autoservisRepo.existsById(autoservisId))
            throw new MissingEntityException(Autoservis.class, autoservisId);

        if (autoservisRepo.existsByOibAndIdNot(autoservis.getOib(), autoservisId))
            throw new RequestDeniedException(
                    "Autoservis with OIB " + autoservis.getOib() + " already exists");
        if (autoservisRepo.existsByEmailAndIdNot(autoservis.getEmail(), autoservisId))
            throw new RequestDeniedException(
                    "Autoservis with email " + autoservis.getEmail() + " already exists");
        if (autoservisRepo.existsByCompanyNameAndIdNot(autoservis.getCompanyName(), autoservisId))
            throw new RequestDeniedException(
                    "Autoservis with companyName " + autoservis.getCompanyName() + " already exists");

        autoservisRepo.save(autoservis);

        return autoservis;
    }

    @Override
    public List<User> listAllMechanics(String companyName) {
        Optional<Autoservis> autoservisOpt = autoservisRepo.findByCompanyName(companyName);
        Assert.isTrue(autoservisOpt.isPresent(), "Autoservis  " + companyName + " does not exist");
        Autoservis autoservis = autoservisOpt.get();

        Role mechanicRole = roleRepo.findById((long) Role.AUTO_MECHANIC).orElseThrow();

        return autoservis.getUsers().stream()
                .filter(user -> user.getRoles().contains(mechanicRole))
                .sorted(Comparator.comparing(User::getLastName))
                .collect(Collectors.toList());
    }

    @Override
    public Autoservis removeMechanic(String companyName, String username) {
        System.out.println("Removing part..");
        Assert.notNull(username, "Company name must be given");
        Optional<Autoservis> autoservisOptional = autoservisRepo.findByCompanyName(companyName);
        Assert.isTrue(autoservisOptional.isPresent(), "Autoservis " + companyName + " does not exist");
        Autoservis autoservis = autoservisOptional.get();

        Assert.notNull(username, "Username must be given");
        Optional<User> userOptional = userRepo.findByUsername(username);
        Assert.isTrue(userOptional.isPresent(), "User " + username + " does not exist");
        User user = userOptional.get();

        List<User> mechanics = autoservis.getUsers();
        if (mechanics == null || !mechanics.contains(user)) {
            throw new RequestDeniedException(
                    "Autoservis " + companyName + " does not have mechanic " + username);
        }

        System.out.println("Made it here");
        mechanics.remove(user);
        System.out.println("Made it here 2");
        autoservis.setUsers(mechanics);
        System.out.println("Made it here 3");

        user.setAutoservis(null);
        System.out.println("Made it here 4");
        Set<Role> roles = new LinkedHashSet<>();
        System.out.println("Made it here 5");
        Role mechanicRole = roleRepo.findById((long) Role.AUTO_MECHANIC).orElseThrow();
        System.out.println("Made it here 6");
        roles.add(mechanicRole);
        System.out.println("Made it here 7");
        user.setRoles(roles);

        userRepo.save(user);
        autoservisRepo.save(autoservis);
        return autoservis;
    }

    @Override
    public List<Autoservis> getAll() {
        return autoservisRepo.findAll();
    }

    private void validate(Autoservis autoservis) {
        Assert.notNull(autoservis, "Autoservis must not be null");
        Assert.hasText(autoservis.getCompanyName(), "Company name must be given");
        Assert.hasText(autoservis.getEmail(), "Email must be given");
        Assert.notNull(autoservis.getLocation(), "Location must not be null");
        String oib = autoservis.getOib();
        Assert.hasText(oib, "OIB must be given");
        Assert.isTrue(oib.matches(OIB_FORMAT),
                "OIB must have 11 digits. OIB entered has " + oib.length() + " digits.");

    }
}
