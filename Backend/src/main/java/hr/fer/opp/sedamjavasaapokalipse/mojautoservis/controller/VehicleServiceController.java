package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;


import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Service;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.VehicleService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.VehicleServicePartService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.VehicleServiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/vehicle-service")
public class VehicleServiceController {

    private final VehicleServiceService vehicleServiceService;

    public VehicleServiceController(VehicleServiceService vehicleServiceService) {
        this.vehicleServiceService = vehicleServiceService;
    }

    @GetMapping("/by_mechanic/{mechanicName}")
    private List<VehicleService> retrieveVehicleServicesByMechanic(@PathVariable String mechanicName) {
        return vehicleServiceService.listAllByMechanicAndOpenOrder(mechanicName);
    }

    @GetMapping("/by_user/{username}")
    private List<VehicleService> retrieveVehicleServicesByUser(@PathVariable String username) {
        return vehicleServiceService.listAllByUserAndOpenOrder(username);
    }

    @PostMapping("")
    private ResponseEntity<VehicleService> createVehicleService(@RequestBody VehicleService vehicleService) {
        VehicleService created = vehicleServiceService.createVehicleService(vehicleService);
        return ResponseEntity.created(URI.create("/" + created.getServiceId())).body(created);
    }

    @PatchMapping("/{id}")
    public VehicleService updateVehicleService(@PathVariable long id, @RequestBody VehicleServicePartService vehicleServicePartService) {
        VehicleService vehicleService = vehicleServicePartService.getVehicleService();
        if(!vehicleService.getServiceId().equals(id))
            throw new IllegalArgumentException("Student ID must be preserved");
        Set<VehicleServicePartService> vehicleServicePartServices = vehicleService.getVehicleServicePartServices();
        if(vehicleServicePartServices == null) {
            vehicleServicePartServices = new LinkedHashSet<>();
        }
        vehicleServicePartServices.add(vehicleServicePartService);
        vehicleService.setVehicleServicePartServices(vehicleServicePartServices);
        return vehicleServiceService.updateVehicleService(vehicleService);
    }
}
