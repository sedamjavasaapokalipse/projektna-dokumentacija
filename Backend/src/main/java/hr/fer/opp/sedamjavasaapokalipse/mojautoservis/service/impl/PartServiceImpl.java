package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.PartRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.MissingEntityException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.PartService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PartServiceImpl implements PartService {

    private final PartRepository partRepo;
    private final AutoservisService autoservisService;

    public PartServiceImpl(PartRepository partRepo, AutoservisService autoservisService) {
        this.partRepo = partRepo;
        this.autoservisService = autoservisService;
    }

    @Override
    public Part fetch(Long id) {
        return partRepo.findById(id).orElseThrow(() -> new MissingEntityException(Part.class, id));
    }

    @Override
    public Part createPart(String companyName, Part part) {
        validate(part);
        Assert.isNull(part.getId(), "Part ID must be null. Its value: " + part.getId());

        Optional<Autoservis> autoservisOptional = autoservisService.findByCompanyName(companyName);
        Assert.isTrue(autoservisOptional.isPresent(), "Autoservis " + companyName + " does not exist");
        Autoservis autoservis = autoservisOptional.get();   //check if part already exists?
        Set<Part> parts = autoservis.getParts() == null ? new LinkedHashSet<>() : autoservis.getParts();

        parts.add(part);
        autoservis.setParts(parts);
        Set<Autoservis> autoservisi = new LinkedHashSet<>();
        autoservisi.add(autoservis);
        part.setAutoservisi(autoservisi);

        return partRepo.save(part);
    }

    @Override
    public Part updatePart(Long id, Part newPart) {
        validate(newPart);
        Assert.notNull(id, "Id must be given");
        Optional<Part> optionalPart= partRepo.findById(id);
        Assert.isTrue(optionalPart.isPresent(), "Part does not exist");
        Part oldPart = optionalPart.get();

        newPart.setAutoservisi(oldPart.getAutoservisi());
        newPart.setId(oldPart.getId());

        partRepo.save(newPart);
        return newPart;
    }

    @Override
    public Optional<Part> findByPartName(String partName) {
        Assert.notNull(partName, "Part name must be given");
        return partRepo.findByPartName(partName);
    }

    @Override
    public List<Part> listAllByCompanyName(String companyName) {
        Assert.notNull(companyName, "Company name must be given");

        Optional<Autoservis> autoservisOptional = autoservisService.findByCompanyName(companyName);
        Assert.isTrue(autoservisOptional.isPresent(), "Autoservis " + companyName + " does not exist");
        Autoservis autoservis = autoservisOptional.get();
        Set<Part> partSet = autoservis.getParts() == null ? new HashSet<>() : autoservis.getParts();

        List<Part> parts = new ArrayList<>(autoservis.getParts());
        parts.sort(Comparator.comparing(Part::getPartName));
        return parts;
    }

    @Override
    public List<Part> listAllByMechanicUsername(String mechanicUsername) {
        Autoservis company = autoservisService.findByMechanicUsername(mechanicUsername).orElseThrow();
        String companyName = company.getCompanyName();
        return listAllByCompanyName(companyName);
    }

    @Override
    public Part deletePart(Long id) {
        Assert.notNull(id, "Part ID must be given");
        Optional<Part> partOpt = partRepo.findById(id);

        Assert.isTrue(partOpt.isPresent(), "Part with ID " + id + " does not exist");
        Part part = partOpt.get();

        partRepo.delete(part);
        return part;
    }

    private void validate(Part part) {
        Assert.notNull(part, "Part must not be null");
        Assert.hasText(part.getPartName(), "Part name must be given");
        Double price = part.getPrice();
        Double estKilometers = part.getEstimatedKilometers();
        Assert.notNull(price, "Part must have price");
        Assert.notNull(estKilometers, "Part must have estimated kilometers");
        Assert.isTrue(price >= 0, "Price must be a positive number");
        Assert.isTrue(estKilometers >= 0, "Estimated kilometers must be a positive number");
    }
}
