package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByOib(String oib);

    int countByOib(String oib);

    boolean existsByOibAndIdNot(String oib, Long id);

    boolean existsByEmailAndIdNot(String email, Long id);

    boolean existsByUsernameAndIdNot(String username, Long id);

    void deleteByUsername(String username);

    /*@Query("select new hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle(v.brand, v.registrationMark) from User u left join u.vehicles v")
    Set<Vehicle> findAll(@Param("s") String username);*/

}
