package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration;

public enum ServiceType {
    REGULAR,
    EMERGENCY
}
