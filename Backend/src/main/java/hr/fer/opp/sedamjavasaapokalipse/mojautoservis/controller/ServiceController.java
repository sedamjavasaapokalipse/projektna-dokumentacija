package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Service;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/services")
public class ServiceController {

    private final ServiceService serviceService;

    @Autowired
    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @GetMapping("/{companyName}")
    public Set<Service> listServices(@PathVariable String companyName) {
        return serviceService.listAllByCompanyName(companyName);
    }

    @GetMapping("/by-mechanic/{mechanicUsername}")
    public Set<Service> retrieveAllByMechanic(@PathVariable String mechanicUsername) {
        return serviceService.listAllByMechanicUsername(mechanicUsername);
    }

    @GetMapping("/{id}")
    public Service fetch(@PathVariable long id) {
        return serviceService.fetch(id);
    }

    @PostMapping("/create/{companyName}")
    public ResponseEntity<Service> createService(@PathVariable String companyName, @RequestBody Service service) {
        Service created = serviceService.createService(companyName, service);
        return new ResponseEntity<>(created, HttpStatus.OK);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Service> updateService(@PathVariable Long id, @RequestBody Service service) {
        Service updated = serviceService.updateService(id, service);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Service> deleteService(@PathVariable Long id) {
        Service deleted = serviceService.deleteService(id);
        return new ResponseEntity<>(deleted, HttpStatus.OK);
    }
}
