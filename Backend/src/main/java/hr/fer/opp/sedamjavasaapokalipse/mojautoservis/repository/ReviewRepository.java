package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Long> {
}
