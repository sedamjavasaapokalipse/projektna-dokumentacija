package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import org.springframework.stereotype.Service;

@Service
public interface HuoService {

    boolean existsByRegistrationMark(String registrationMark);

}
