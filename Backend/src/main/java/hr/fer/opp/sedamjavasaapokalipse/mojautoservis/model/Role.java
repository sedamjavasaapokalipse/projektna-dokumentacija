package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
public class Role {

    public static final int REGISTERED_USER = 1;
    public static final int AUTO_MECHANIC = 2;
    public static final int ADMIN = 3;
    public static final int SUPERUSER = 4;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String roleName;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

}
