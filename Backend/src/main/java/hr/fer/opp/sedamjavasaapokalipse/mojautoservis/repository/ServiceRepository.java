package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

@org.springframework.stereotype.Service
public interface ServiceRepository extends JpaRepository<Service, Long> {

    Optional<Service> findByServiceName(String serviceName);
}
