package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Service;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.AutoservisRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.ServiceRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.MissingEntityException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.RequestDeniedException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.ServiceService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    private ServiceRepository serviceRepo;
    private AutoservisRepository autoservisRepo;
    private AutoservisService autoservisService;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepo, AutoservisRepository autoservisRepo,
                              AutoservisService autoservisService) {
        this.serviceRepo = serviceRepo;
        this.autoservisRepo = autoservisRepo;
        this.autoservisService = autoservisService;
    }

    @Override
    public Set<Service> listAllByCompanyName(String companyName) {

        Autoservis autoservis = autoservisRepo.findByCompanyName(companyName).orElseThrow(() ->
                new RequestDeniedException("No company with company name:" + companyName));
        return autoservis.getServices() == null ? new LinkedHashSet<>() : autoservis.getServices();
    }

    @Override
    public Set<Service> listAllByMechanicUsername(String mechanicUsername) {
        Autoservis company = autoservisService.findByMechanicUsername(mechanicUsername).orElseThrow();
        String companyName = company.getCompanyName();
        return listAllByCompanyName(companyName);
    }

    @Override
    public Service fetch(Long id) {
        return serviceRepo.findById(id).orElseThrow(() -> new MissingEntityException(Service.class, id));
    }

    @Override
    public Service createService(String companyName, Service service) {

        validate(service);
        Assert.isNull(service.getId(), "Service id must be null. Given id: " + service.getId());

        Autoservis autoservis = autoservisRepo.findByCompanyName(companyName).orElseThrow(() ->
                new RequestDeniedException("No company with company name:" + companyName));

        Set<Service> services = autoservis.getServices() == null ? new LinkedHashSet<>() : autoservis.getServices();
        services.add(service);
        autoservis.setServices(services);
        service.setAutoservis(autoservis);

        return serviceRepo.save(service);
    }

    @Override
    public Service updateService(Long id, Service newService) {

        validate(newService);
        Assert.notNull(id, "Id must be given!");

        Service oldService = fetch(id);

        newService.setId(oldService.getId());
        newService.setAutoservis(oldService.getAutoservis());

        serviceRepo.save(newService);
        return newService;
    }

    @Override
    public Service deleteService(Long id) {

        Assert.notNull(id, "Id must be given!");

        Service service = fetch(id);
        serviceRepo.delete(service);

        return service;
    }

    @Override
    public Optional<Service> findByServiceName(String serviceName) {
        Assert.notNull(serviceName, "Service name must not be null!");
        return serviceRepo.findByServiceName(serviceName);
    }

    private void validate(Service service) {
        Assert.notNull(service, "Service must not be null!");
        Assert.hasText(service.getServiceName(), "Service name must be given!");
        Double price = service.getPrice();
        Assert.notNull(price, "Price must not be null!");
        Assert.isTrue(price > 0, "Price must be a positive number!");
    }
}
