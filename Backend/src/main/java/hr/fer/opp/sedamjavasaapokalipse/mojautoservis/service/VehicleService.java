package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public interface VehicleService {


    Set<Vehicle> listAll(String username);

    Vehicle fetch(String regOzn);

    Optional<Vehicle> findByRegistrationMark(String registrationMark);

    Vehicle createVehicle(Vehicle vehicle, String username);

    Vehicle updateVehicle(Long id, Vehicle newVehicle);

    Optional<Vehicle> getVehicle(String registrationMark);

    void deleteByRegistrationMark(String registrationMark);
}
