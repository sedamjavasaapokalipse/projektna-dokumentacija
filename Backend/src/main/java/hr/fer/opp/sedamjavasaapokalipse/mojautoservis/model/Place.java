package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Place {

    @Id
    private Long postalCode;

    @NotNull
    private String placeName;

}
