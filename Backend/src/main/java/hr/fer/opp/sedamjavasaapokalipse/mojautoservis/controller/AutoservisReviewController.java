package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Review;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.ReviewService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/reviews")
public class AutoservisReviewController {

    private final ReviewService reviewService;

    public AutoservisReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @PostMapping("")
    public ResponseEntity<Review> postReview(@RequestParam("companyName") String companyName,
                                             @RequestParam("username") String username,
                                             @RequestBody Review review) {

        Review created = reviewService.createReview(review, username, companyName);
        return ResponseEntity.created(URI.create("/" + created.getId())).body(created);
    }

    @GetMapping("/{companyName}")
    public ResponseEntity<List<Review>> listAll(@PathVariable String companyName) {
        return new ResponseEntity<>(reviewService.listAll(companyName), HttpStatus.OK);
    }

}
