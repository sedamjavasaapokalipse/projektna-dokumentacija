package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@ToString(exclude = "autoservisi")
@EqualsAndHashCode(exclude = "autoservisi")
public class Part {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String partName;

    @NotNull
    private Double price;

    @NotNull
    private Double estimatedKilometers;

    @JsonIgnore
    @ManyToMany(mappedBy = "parts")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Autoservis> autoservisi;

}
