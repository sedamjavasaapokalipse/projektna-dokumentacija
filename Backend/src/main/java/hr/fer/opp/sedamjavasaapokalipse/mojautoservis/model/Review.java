package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
//enables that one user can only one time review one autoservice
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"user_id", "autoservis_id"})
})
@ToString(exclude = {"user", "autoservis"})
@EqualsAndHashCode(exclude = {"user", "autoservis"})
/*@IdClass(ReviewId.class)*/
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "user_id", nullable=false)
    @JsonBackReference(value="user-reviews")
    private User user;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "autoservis_id", nullable=false)
    @JsonBackReference(value="autoservis-reviews")
    private Autoservis autoservis;

    @NotNull
    private int rate;

    @NotNull
    private String comment;
}
