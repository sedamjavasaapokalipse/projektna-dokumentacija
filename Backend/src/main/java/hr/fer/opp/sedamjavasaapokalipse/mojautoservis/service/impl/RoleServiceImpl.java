package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Role;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.RoleRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.MissingEntityException;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepo;

    public RoleServiceImpl(RoleRepository roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public List<Role> listAll() {
        return roleRepo.findAll();
    }

    @Override
    public Role fetch(long id) {
        return roleRepo.findById(id).orElseThrow(() -> new MissingEntityException(Role.class, id));
    }
}
