package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Review;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository.ReviewRepository;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.AutoservisService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.ReviewService;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    private final UserService userService;
    private final AutoservisService autoservisService;
    private final ReviewRepository reviewRepository;

    public ReviewServiceImpl(UserService userService, AutoservisService autoservisService, ReviewRepository reviewRepository) {
        this.userService = userService;
        this.autoservisService = autoservisService;
        this.reviewRepository = reviewRepository;
    }


    @Override
    @Transactional
    public Review createReview(Review review, String username, String companyName) {
        validate(review);
        User user = userService.getUser(username).orElse(null);
        Autoservis autoservis = autoservisService.findByCompanyName(companyName).orElse(null);
        Assert.notNull(user, "user not found ");
        Assert.notNull(autoservis, "car service not found");

        /*List<Review> userReviews =  user.getReviews() == null ? new ArrayList<>() : user.getReviews();
        List<Review> autoserviceReviews = autoservis.getReviews() == null ? new ArrayList<>()  : autoservis.getReviews();
        autoserviceReviews.add(review);
        autoservis.setReviews(autoserviceReviews);
        userReviews.add(review);
        user.setReviews(userReviews);*/

        review.setAutoservis(autoservis);
        review.setUser(user);

        //provjeriti jesu li potrebne ove dvije linije koda
        /*autoservisService.updateAutoservis(autoservis);
        userService.updateUser(user);*/


        return reviewRepository.save(review);
    }

    @Override
    public List<Review> listAll(String companyName) {
        Autoservis autoservis = autoservisService.findByCompanyName(companyName).orElse(null);
        Assert.notNull(autoservis, "car service not found");


        return autoservis.getReviews();
    }

    private void validate(Review review) {
        Assert.notNull(review, "review je null");
        Assert.hasText(review.getComment(), "komentar mora sadržavati tekst");
        Assert.isTrue(inRange(review.getRate()), "ocjena mora biti od 0 do 5");
    }

    private boolean inRange(int rate) {
        return rate >= 0 && rate <= 5;
    }
}
