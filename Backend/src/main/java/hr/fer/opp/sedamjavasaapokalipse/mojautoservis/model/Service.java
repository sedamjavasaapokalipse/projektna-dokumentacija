package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@ToString(exclude = "autoservis")
@EqualsAndHashCode(exclude = "autoservis")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String serviceName;

    @NotNull
    private Double price;

    private String description;

    @ManyToOne
    @JoinColumn(name = "autoservis_id")
    @JsonBackReference
    private Autoservis autoservis;
}
