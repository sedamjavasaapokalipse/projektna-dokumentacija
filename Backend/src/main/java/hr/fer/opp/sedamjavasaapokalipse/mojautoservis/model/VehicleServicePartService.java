package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class VehicleServicePartService implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="vehicle_service_id")
    private VehicleService vehicleService;

    @Id
    @ManyToOne
    @JoinColumn(name="service_id")
    private Service service;

    @Id
    @ManyToOne
    @JoinColumn(name="part_id")
    private Part part;
}
