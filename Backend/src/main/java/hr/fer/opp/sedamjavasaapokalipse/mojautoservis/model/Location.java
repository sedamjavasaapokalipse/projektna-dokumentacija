package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"address","place_id"}))
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "place_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private Place place;

    @NotNull
    private String address;
}
