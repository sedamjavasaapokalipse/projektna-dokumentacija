package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.repository;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Location;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface LocationRepository extends JpaRepository<Location, Long> {

    Optional<Location> findFirstByPlace(Place place);
}
