package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.enumeration.ServiceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
@ToString(exclude = "autoservis")
@EqualsAndHashCode(exclude = "autoservis")
public class VehicleService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long serviceId;

    private Integer kilometersPassed;

    private LocalDateTime timeStarted;

    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    @ManyToOne
    @JoinColumn(name = "autoservis_id")
    @JsonBackReference(value = "autoservis-vehicleService")
    private Autoservis autoservis;

    @ManyToOne
    @JoinColumn(name = "registration_mark")
    private Vehicle vehicle;

    @OneToOne(mappedBy = "vehicleService", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Order order;

    @OneToMany(mappedBy = "vehicleService", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<VehicleServicePartService> vehicleServicePartServices;
}
