package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.controller;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("")
    public ResponseEntity<Set<Vehicle>> listVehicles(@RequestParam(value="username") String username){
        return new ResponseEntity<>(vehicleService.listAll(username), HttpStatus.OK);
    }

    @GetMapping("/{registrationMark}")
    public Vehicle getVehicle(@PathVariable String registrationMark){
         return vehicleService.fetch(registrationMark);
    }

    @PostMapping("/{username}")
    public ResponseEntity<Vehicle> createVehicle(@PathVariable String username, @RequestBody Vehicle vehicle) {
        System.out.println("Pozivam se!");
        Vehicle created = vehicleService.createVehicle(vehicle, username);
        return ResponseEntity.created(URI.create("/" + created.getRegistrationMark())).body(created);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Vehicle> updateVehicle(@PathVariable Long id, @RequestBody Vehicle vehicle) {
        Vehicle updated = vehicleService.updateVehicle(id, vehicle);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @Transactional
    @DeleteMapping("/{registrationMark}")
    public ResponseEntity<?> deleteVehicle(@PathVariable String registrationMark) {
        vehicleService.deleteByRegistrationMark(registrationMark);
        return ResponseEntity.ok().build();
    }

    @GetMapping("available")
    public ResponseEntity<Boolean> isRegistrationMarkAvailable(@RequestParam("registrationMark") String registrationMark) {
        Assert.notNull(registrationMark, "Registration mark must not be null");
        return new ResponseEntity<>(vehicleService.findByRegistrationMark(registrationMark).isEmpty(), HttpStatus.OK);
    }

}
