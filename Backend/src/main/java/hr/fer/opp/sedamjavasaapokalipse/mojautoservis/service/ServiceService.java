package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Service;

import java.util.Optional;
import java.util.Set;

@org.springframework.stereotype.Service
public interface ServiceService {

    Set<Service> listAllByCompanyName(String companyName);

    Set<Service> listAllByMechanicUsername(String mechanicUsername);

    Service fetch(Long id);

    Service createService(String companyName, Service service);

    Service updateService(Long id, Service newService);

    Service deleteService(Long id);

    Optional<Service> findByServiceName(String serviceName);
}
