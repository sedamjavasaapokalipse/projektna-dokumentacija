package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Location;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Place;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest
class AutoservisServiceImplTest {

    @Autowired
    AutoservisServiceImpl autoservisService;

    @Test
    @Transactional
    void createAutoservisTest() {
        Autoservis autoservis = new Autoservis();

        Autoservis createdAutoservis = createAutoservice(autoservis);

        Assertions.assertEquals(createdAutoservis, autoservis);
    }

    @Test
    @Transactional
    void createAutoservisNullTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> autoservisService.createAutoservis(null));
    }

    @Test
    @Transactional
    void updateAutoservisNullTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> autoservisService.updateAutoservis(null));
    }

    @Transactional
    @Test
    void updateAutoservisTest() {
        Autoservis autoservis = new Autoservis();
        Autoservis createdAutoservis = createAutoservice(autoservis);

        createdAutoservis.setEmail("a.b@gmail.com");

        Autoservis updatedAutoservis = autoservisService.updateAutoservis(createdAutoservis);

        Assertions.assertEquals(updatedAutoservis, createdAutoservis);
    }

    @Transactional
    @Test
    void validateTest() {
        Autoservis autoservis = new Autoservis();
        Autoservis createdAutoservis = createAutoservice(autoservis);

        createdAutoservis.setEmail("a.b.com");

        Assertions.assertThrows(ConstraintViolationException.class, () -> autoservisService.updateAutoservis(createdAutoservis));

        createdAutoservis.setEmail("");
        Assertions.assertThrows(IllegalArgumentException.class, () -> autoservisService.updateAutoservis(createdAutoservis));

        createdAutoservis.setEmail("a.b@gmail.com");
        createdAutoservis.setCompanyName(null);
        Assertions.assertThrows(IllegalArgumentException.class, () -> autoservisService.updateAutoservis(createdAutoservis));
    }


    Autoservis createAutoservice(Autoservis autoservis) {
        autoservis.setCompanyName("BMW Service");
        autoservis.setEmail("bmw.service@gmail.com");

        Location location = new Location();
        location.setAddress("Unska 3");

        Place place = new Place();
        place.setPlaceName("Zagreb");
        place.setPostalCode(10000L);

        location.setPlace(place);

        autoservis.setLocation(location);
        autoservis.setOib("12345678910");

        return autoservisService.createAutoservis(autoservis);
    }

}
