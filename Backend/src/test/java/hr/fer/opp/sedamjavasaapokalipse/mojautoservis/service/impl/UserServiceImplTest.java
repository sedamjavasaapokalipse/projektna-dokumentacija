package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;


import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceImplTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserServiceImpl userService;

    @Transactional
    @Test
    void createUserTest() {
        User user = new User();

        User createdUser = createUser(user);

        Assertions.assertEquals(createdUser, user);
    }

    @Transactional
    @Test
    void findByUsernameTest() {
        User user = new User();

        createUser(user);

        User user1 = userService.findByUsername("iivic").orElse(null);

        Assertions.assertEquals(user1, user);
    }

    @Transactional
    @Test
    void updateUserNullTest() {
        User user = new User();
        createUser(user);

        Assertions.assertThrows(IllegalArgumentException.class, () -> userService.updateUser(null));
    }

    @Transactional
    @Test
    void updateUserTest() {
        User user = new User();
        User createdUser = createUser(user);

        createdUser.setUsername("ivoivic");
        createdUser.setOib("12345678911");
        createdUser.setFirstName("Ivan");

        User updatedUser = userService.updateUser(createdUser);

        Assertions.assertEquals(updatedUser, createdUser);
    }

    @Transactional
    @Test
    void validateTest() {
        User user = new User();
        User createdUser = createUser(user);

        createdUser.setOib("12414");

        Assertions.assertThrows(IllegalArgumentException.class, () -> userService.updateUser(createdUser));
    }

    @Transactional
    @Test
    void validateTest2() {
        User user = new User();
        User createdUser = createUser(user);

        createdUser.setEmail("a.b.hr");

        Assertions.assertThrows(ConstraintViolationException.class, () -> userService.updateUser(createdUser));
    }

    User createUser(User user) {
        user.setEmail("ivo.ivic@gmail.com");
        user.setFirstName("Ivo");
        user.setLastName("Ivić");
        user.setOib("12345678910");
        user.setUsername("iivic");
        user.setPassword(passwordEncoder.encode("123456"));

        return userService.createUser(user);
    }

}
