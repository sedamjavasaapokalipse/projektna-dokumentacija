package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Autoservis;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Location;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Part;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Place;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
class PartServiceImplTest {

    @Autowired
    PartServiceImpl partService;

    @Autowired
    AutoservisServiceImpl autoservisService;

    @Test
    @Transactional
    void createPartTest() {
        Part part = new Part();

        createAutoservice();

        Part createdPart = createPart(part);

        Assertions.assertEquals(createdPart, part);
    }

    @Test
    @Transactional
    void updatePartNullIdTest() {
        Part part = new Part();

        createAutoservice();
        createPart(part);

        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(null, part));
    }

    @Transactional
    @Test
    void updatePartNullPartTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(1L, null));
    }

    @Test
    @Transactional
    void validatePartTest() {
        Part part = new Part();
        createAutoservice();
        createPart(part);

        part.setPrice(-10.0);
        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(1L, part));

        part.setPartName("");
        part.setPrice(20.0);
        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(1L, part));

        part.setPartName("turbocharger");
        part.setEstimatedKilometers(-1.0);
        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(1L, part));

        part.setEstimatedKilometers(2.0);
        part.setPartName(null);
        Assertions.assertThrows(IllegalArgumentException.class, () -> partService.updatePart(1L, part));
    }

    /**
     * It would be better to make beforeAll method,
     * but beforeAll method cannot me annotated with
     * \@Transactional so it fails.
     */
    void createAutoservice() {
        Autoservis autoservis = new Autoservis();

        autoservis.setCompanyName("BMW Service");
        autoservis.setEmail("bmw.service@gmail.com");

        Location location = new Location();
        location.setAddress("Unska 3");

        Place place = new Place();
        place.setPlaceName("Zagreb");
        place.setPostalCode(10000L);

        location.setPlace(place);

        autoservis.setLocation(location);
        autoservis.setOib("12345678910");

        autoservisService.createAutoservis(autoservis);
    }

    Part createPart(Part part) {
        part.setEstimatedKilometers(10000000.0);
        part.setPartName("engine");
        part.setPrice(100000.0);

        return partService.createPart("BMW Service", part);
    }

}
