package hr.fer.opp.sedamjavasaapokalipse.mojautoservis.service.impl;

import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.User;
import hr.fer.opp.sedamjavasaapokalipse.mojautoservis.model.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
class VehicleServiceImplTest {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    VehicleServiceImpl vehicleService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    @Transactional
    void createVehicleTest() {
        Vehicle vehicle = new Vehicle();
        vehicle.setRegistrationMark("ZG123ač");
        vehicle.setBrand("bmw");
        vehicle.setModel("m5");
        vehicle.setUser(getUser());

        Vehicle createVehicle = vehicleService.createVehicle(vehicle, "iivic");

        Assertions.assertEquals(createVehicle, vehicle);
    }

    @Transactional
    @Test
    void checkValidate() {
        Vehicle vehicle = new Vehicle();
        vehicle.setUser(getUser());
        vehicle.setRegistrationMark("");
        vehicle.setBrand("audi");
        vehicle.setModel("a4");

        Assertions.assertThrows(IllegalArgumentException.class, () -> vehicleService.createVehicle(vehicle, "iivic"));
    }

    @Transactional
    @Test
    void checkValidate2() {
        Vehicle vehicle = new Vehicle();
        vehicle.setUser(getUser());
        vehicle.setRegistrationMark("zg123a-");
        vehicle.setModel("cls");
        vehicle.setBrand("mercedes");

        Assertions.assertThrows(IllegalArgumentException.class, () -> vehicleService.createVehicle(vehicle, "iivic"));
    }

    User getUser() {
        User user = new User();
        user.setEmail("ivo.ivic@gmail.com");
        user.setFirstName("Ivo");
        user.setLastName("Ivić");
        user.setOib("12345678910");
        user.setUsername("iivic");
        user.setPassword(passwordEncoder.encode("123456"));

        return userService.createUser(user);
    }

}
