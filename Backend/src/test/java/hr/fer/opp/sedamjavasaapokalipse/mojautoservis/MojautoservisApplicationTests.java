package hr.fer.opp.sedamjavasaapokalipse.mojautoservis;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Chrome driver can be downloaded from : https://chromedriver.storage.googleapis.com/index.html
 */
class MojautoservisApplicationTests {
    private WebDriver driver;

    @BeforeEach
    void initDriver() {
        String pathToChromeDriver = "";

        System.setProperty("webdriver.chrome.driver", pathToChromeDriver);

        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://localhost:3000/");
        driver.manage().window().maximize();
    }

    @AfterEach
    void quitDriver() {
        driver.quit();
    }

    /**
     * It is expected that user with username : iivic and
     * password : 12345 exists in database.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void checkLogin() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[1]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"id01\"]/form/div/input[1]")).sendKeys("iivic");
        driver.findElement(By.xpath("//*[@id=\"psw\"]")).sendKeys("123456");

        WebElement buttonLogin = driver.findElement(By.xpath("//*[@id=\"id01\"]/form/button"));
        buttonLogin.click();

        Thread.sleep(1000); // It is better to use WaitDriverWait

        Assertions.assertEquals("Korisničko ime: iivic", driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/p")).getText());
    }

    /**
     * It is expected that user Pero Peric in not present in database.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void checkRegister() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[2]/button")).click();

        driver.findElement(By.xpath("//*[@id=\"fname\"]")).sendKeys("Pero");
        driver.findElement(By.xpath("//*[@id=\"lname\"]")).sendKeys("Peric");
        driver.findElement(By.xpath("//*[@id=\"oib\"]")).sendKeys("00000000000");
        driver.findElement(By.xpath("//*[@id=\"uname\"]")).sendKeys("pperic");
        driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("pero.peric@gmail.hr");
        driver.findElement(By.xpath("//*[@id=\"psw\"]")).sendKeys("123456");
        driver.findElement(By.xpath("//*[@id=\"rpsw\"]")).sendKeys("123456");

        Thread.sleep(1000);

        WebElement buttonRegister = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div/form/div/button"));

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView()", buttonRegister);

        buttonRegister.click();

        Thread.sleep(2000);

        Assertions.assertEquals("Korisničko ime: pperic",
                driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/p")).getText());
    }

    /**
     * It is expected that user with username: iivic, password: 123456
     * and first name: Ivo exists in database.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void checkUserInfoChange() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[1]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"id01\"]/form/div/input[1]")).sendKeys("iivic");
        driver.findElement(By.xpath("//*[@id=\"psw\"]")).sendKeys("123456");

        WebElement buttonLogin = driver.findElement(By.xpath("//*[@id=\"id01\"]/form/button"));
        buttonLogin.click();

        Thread.sleep(1000); // It is better to use WaitDriverWait

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/ul/li[3]/button"))
                .click();

        Thread.sleep(1000); // It is better to use WaitDriverWait

        driver.findElement(By.xpath("//*[@id=\"fname\"]"))
                .sendKeys("a");

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/button[1]"))
                .click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/ul/li[3]/button"))
                .click();

        Assertions.assertEquals(driver.findElement(By
                .xpath("//*[@id=\"fname\"]"))
                .getAttribute("value"), "Ivoa");
    }

    /**
     * It is expected that user with username: iivic, password: 123456
     * and first name: Ivo exists in database.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void checkVehicleAdding() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[1]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"id01\"]/form/div/input[1]")).sendKeys("iivic");
        driver.findElement(By.xpath("//*[@id=\"psw\"]")).sendKeys("123456");

        WebElement buttonLogin = driver.findElement(By.xpath("//*[@id=\"id01\"]/form/button"));
        buttonLogin.click();

        Thread.sleep(1000); // It is better to use WaitDriverWait
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/ul/li[1]/button")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/button")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"brand\"]"))
                .sendKeys("bmw");
        driver.findElement(By.xpath("//*[@id=\"model\"]"))
                .sendKeys("m5");
        driver.findElement(By.xpath("//*[@id=\"reg\"]"))
                .sendKeys("ZG4836RE");

        driver.findElement(By.xpath("//*[@id=\"id01\"]/form/button")).click();

        Thread.sleep(1000);
        Assertions.assertEquals(driver.findElement(By
                .xpath("//*[@id=\"root\"]/div/div[3]/ul/li/button/b"))
                .getText(), "bmw m5");
    }

    /**
     * Testing for edge case.
     * <p>
     * It is expected that user with username: iivic, password: 123456 exists in database.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void checkLoginWrongPass() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[1]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"id01\"]/form/div/input[1]")).sendKeys("iivic");
        driver.findElement(By.xpath("//*[@id=\"psw\"]")).sendKeys("1234567890");

        WebElement buttonLogin = driver.findElement(By.xpath("//*[@id=\"id01\"]/form/button"));
        buttonLogin.click();

        Thread.sleep(1000);

        Assertions.assertEquals(driver.findElement(By.xpath("//*[@id=\"id01\"]/form/div/label[3]")).getText(), "Pogrešno korisničko ime ili lozinka!");
    }


    /**
     * Testing unimplemented feature.
     *
     * @throws InterruptedException If any thread has interrupted the current thread.
     *                              The interrupted status of the current thread is cleared
     *                              when this exception is thrown.
     */
    @Test
    void testForgotPassword() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav\"]/span/li[1]/button")).click();

        WebElement forgotPassHref = driver.findElement(By.xpath("//*[@id=\"id01\"]/span[2]/a"));

        forgotPassHref.click();

        Thread.sleep(1000);

        WebElement element = driver.findElement(By.xpath("//*[@id=\"id01\"]/span[2]/a"));

        // After first click on forgot password link,
        // when we try to click again, ElementClickInterceptedException should be
        // caused.
        Assertions.assertThrows(ElementClickInterceptedException.class, element::click);
    }
}
