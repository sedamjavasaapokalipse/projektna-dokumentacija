import React, {useEffect, useState} from 'react';
import AuthenticationService from "../service/AuthenticationService";
import VehicleDataService from "../service/VehicleDataService";
import AutoservisDataService from "../service/AutoservisDataService";
import OrderDataService from "../service/OrderDataService";
import {useFormInput} from "./Register";

export default function Order() {
    const [allVehicles, setAllVehicles] = useState(null);
    const [allCompanies, setAllCompanies] = useState(null);
    const companyName = useFormInput("");
    const registrationMark = useFormInput("");

    useEffect(() => {
        if (allVehicles === null) {
            VehicleDataService
                .getVehicles(AuthenticationService.getLoggedInUserName())
                .then(response => setAllVehicles(response.data))
                .catch(error => alert(error.toString()));
        }
    });

    useEffect(() => {
        if (allCompanies === null) {
            AutoservisDataService
                .retrieveAutoservisi()
                .then(response => setAllCompanies(response.data))
                .catch(error => alert(error.toString()));
        }
    });

    function onSubmit(event) {
        event.preventDefault();

        const vehicleService = {
            autoservis: companyName.value,
            registrationMark: registrationMark.value
        };

        console.log(vehicleService);

        OrderDataService
            .createVehicleService(vehicleService)
            .then(() => {
            })
            .catch(error => alert(error.toString()));
    }

    return (
        <div className={"container"}>

            <form onSubmit={onSubmit}>
                <label><b>Izaberi vozilo</b></label>
                <select name="vehicle" value={allVehicles} onChange={registrationMark.onChange}>
                    {allVehicles.map((v) => {
                        return <option key={v.registrationMark} value={v.registrationMark}>{v.registrationMark}</option>;
                    })}
                </select>

                <label><b>Izaberi autoservis</b></label>
                <select name="autoservis" value={allCompanies} onChange={companyName.onChange}>
                    {allCompanies.map((c) => {
                        return <option key={c.id} value={c.companyName}>{c.companyName}</option>;
                    })}
                </select>
                <button type="submit" className="SavePartButton">Otvori nalog</button>
            </form>

        </div>

    )
}