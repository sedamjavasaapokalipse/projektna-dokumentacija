import React, {useState} from 'react';

const WAIT_INTERVAL = 500;

export default function LazyInput(props) {
    const [timeoutId, setTimeoutId] = useState(null);
    const [value, setValue] = useState(props.value);

    function handleChange(e) {
        clearTimeout(timeoutId);

        setValue(e.target.value);
        e.persist();

        setTimeoutId(setTimeout(() => {
            props.onChange(e)
        }, WAIT_INTERVAL))
    }

    return (
        <input type={props.type} placeholder={props.placeholder} id={props.id} value={value}
               step={props.step} className={props.className} onChange={handleChange}/>
    );
}
