import React from 'react'

export default function DeleteConfirmation(props) {

    function confirmDelete() {
        props.confirmDelete();
        props.closePopup()
    }

    function rejectDelete() {
        props.closePopup()
    }

    return (
        <div className='modal'>
            <div className="modal-content">
                <span className="close" onClick={rejectDelete} title="Close Modal">&times;</span>
                <div className="delete">
                    <h2>{props.message}</h2>
                    <button type="button" onClick={confirmDelete} className="cancelbtn"><b>Da</b></button>
                    <button type="button" onClick={rejectDelete} className="cancelbtn"><b>Ne</b></button>
                </div>
            </div>
        </div>
    )
}