import React, {useState} from 'react'

export function useFormInput(initialValue) {
    const [value, setValue] = useState(initialValue);
    const [changed, setChanged] = useState(false);
    const [isValid, setValid] = useState(false);
    const [message, setMessage] = useState('');

    function handleValueChange(event) {
        setChanged(true);
        setValue(event.target.value);
    }

    return {
        value,
        clearValue: () => setValue(''),
        resetValue: () => setValue(initialValue),
        changed,
        setChanged,
        isValid,
        setValid,
        message,
        setMessage,
        onChange: handleValueChange
    }
}