import React from 'react'

export default function FormInputRow(props) {

    const entity = props.entity;

    return (
        <div>
            <label htmlFor={props.id}><b>{props.label}</b></label>
            <input type={props.type} placeholder={props.placeholder}
                   step={props.step} id={props.id} onChange={entity.onChange} value={entity.value}
                   className={!entity.isValid && entity.changed ? "red-border" : ""}/>
            {!entity.isValid && entity.changed &&
            <div className="bad_credentials"><label>{entity.message}</label></div>}
        </div>
    )
}