import React from 'react'
import LazyInput from "./LazyInput";

export default function FormLazyInputRow(props) {

    const entity = props.entity;

    return (
        <div>
            <label htmlFor={props.id}><b>{props.label}</b></label>
            <LazyInput type={props.type} placeholder={props.placeholder}
                       step={props.step} id={props.id} onChange={entity.onChange} value={entity.value}
                       className={!entity.isValid && entity.changed ? "lazy_input red-border" : "lazy_input"}/>
            {!entity.isValid && entity.changed &&
            <div className="bad_credentials"><label>{entity.message}</label></div>}
        </div>
    )
}