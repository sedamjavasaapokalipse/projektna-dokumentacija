import React from 'react'
import '../App.css'
import costumer1 from "../assets/costumer1.svg"
import costumer2 from "../assets/costumer2.svg"

export default function Reviews() {

    return (
        <div className="red-container">
            <div className="container">
                <ul>
                    <li>
                        <figure><img src={costumer1} alt="User Testimonial"/>
                            <blockquote>
                                Sve pohvale, vrlo profesionalno i učinkovito odrađen posao,
                                termin s ljubaznošću dogovoren i rezerviran tjedan dana prije,
                                točno u dogovoreno vrijeme auto preuzet u rad, a na kraju završeno i prije okvirno
                                najavljenog vremena rada.
                                :)
                            </blockquote>
                            <figcaption>- Jane Doe</figcaption>
                        </figure>
                    </li>
                    <li>
                        <figure><img src={costumer2} alt="User Testimonial"/>
                            <blockquote>Jako sam zadovoljan.
                                Iznimno je lako pronaći servis za auto, te se vide najbolji majstori u gradu.
                                Naručio sam servis, došao na vrijeme, te je posao bio obavljen.
                                Svima koji imaju probleme sa svojim automobilom, toplo preporučam ovu stranicu.
                            </blockquote>
                            <figcaption>- John Doe</figcaption>
                        </figure>
                    </li>
                </ul>
            </div>
        </div>
    );
}
