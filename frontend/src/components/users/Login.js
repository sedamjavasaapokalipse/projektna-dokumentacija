import React, {useState} from 'react';
import '../../App.css';
import avatar from '../../assets/iconfinder_7_avatar_2754582.svg'
import AuthenticationService from "../../service/AuthenticationService";
import UserDataService from "../../service/UserDataService";

export default function Login(props) {
    const [usernameOrEmail, setUsernameOrEmail] = useState("");
    const [password, setPassword] = useState("");
    const [badCredentials, setBadCredentials] = useState(false);

    function handleUsernameChange(e) {
        setUsernameOrEmail(e.target.value);
        setBadCredentials(false);
    }

    function handlePasswordChange(e) {
        setPassword(e.target.value);
        setBadCredentials(false);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (usernameOrEmail.includes("@")) {
            UserDataService
                .getUserByEmail(usernameOrEmail)
                .then(response => {
                    let username = response.data.username;
                    submit(username)
                })
                .catch(() => setBadCredentials(true))

        } else {
            submit(usernameOrEmail)
        }
    }

    function submit(username) {
        AuthenticationService
            .executeJwtAuthenticationService(username, password)
            .then(response => {
                AuthenticationService.registerSuccessfulLoginForJwt(username, response.data.token);
                // this.props.history.push('/');
                setBadCredentials(false);
                props.onLogin();
                props.closePopup();
            })
            .catch(() => setBadCredentials(true));
    }

    return (
        <div className='modal'>
            <div className="modal-content" id="id01">
                <div className="imgcontainer">
                    <img src={avatar} alt="Avatar" className="avatar"/>
                </div>

                <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                <form onSubmit={onSubmit}>

                    <div className="login_container">
                        <label><b>Korisničko ime ili email</b></label>
                        <input type="text" placeholder="Unesite korisničko ime ili email" name="username"
                               value={usernameOrEmail}
                               required onChange={handleUsernameChange}/>

                        <label htmlFor="psw"><b>Lozinka</b></label>
                        <input type="password" placeholder="Unesite lozinku" name="password" id="psw"
                               value={password}
                               required onChange={handlePasswordChange}/>

                        {badCredentials &&
                        <label className="bad_credentials">
                            Pogrešno korisničko ime ili lozinka!
                        </label>}
                    </div>
                    <button type="submit" className="LoginButton">Prijava</button>
                </form>

                <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
                <span className="psw"><a href="#">Zaboravljena lozinka?</a></span>
            </div>
        </div>
    )
}
