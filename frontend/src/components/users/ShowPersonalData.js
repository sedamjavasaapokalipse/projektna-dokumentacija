import React, {useEffect, useState} from 'react';
import '../../App.css';
import UserDataService from "../../service/UserDataService";
import AuthenticationService from "../../service/AuthenticationService";
import {useFormInput} from "../form_utils/FormUtils";
import {
    COMPANY_NAME_UNAVAILABLE,
    EMAIL_UNAVAILABLE,
    LOCAL_ATTRIBUTE_USERNAME,
    OIB_UNAVAILABLE,
    PASSWORD_MISMATCH,
    PASSWORD_TOO_SHORT,
    REQUIRED_MESSAGE,
    USERNAME_UNAVAILABLE,
    WRONG_COMPANY_NAME,
    WRONG_EMAIL_MESSAGE,
    WRONG_OIB_MESSAGE, WRONG_OLD_PASSWORD,
    WRONG_POSTAL_CODE,
    WRONG_USERNAME
} from "../../Constants";
import FormInputRow from "../form_utils/FormInputRow";
import FormLazyInputRow from "../form_utils/FormLazyInputRow";
import AutoservisDataService from "../../service/AutoservisDataService";
import DeleteConfirmation from "../form_utils/DeleteConfirmation";

export default function ShowPersonalData(props) {
    const isCompany = props.role.toString() === "ADMIN";

    const firstName = useFormInput(props.user.firstName);
    const lastName = useFormInput(props.user.lastName);
    const userOib = useFormInput(props.user.oib);
    const username = useFormInput(props.user.username);

    const companyName = useFormInput(isCompany ? props.company.companyName : '');
    const companyOib = useFormInput(isCompany ? props.company.oib : '');
    const place = useFormInput(isCompany ? props.company.location.place.placeName : '');
    const postalCode = useFormInput(isCompany ? props.company.location.place.postalCode : '');
    const address = useFormInput(isCompany ? props.company.location.address : '');

    const email = useFormInput(props.user.email);

    const [deleteConfirm, setDeleteConfirm] = useState(false);
    const [showSave, setShowSave] = useState(false);


    useEffect(() => {
        firstName.setValid(firstName.value.length !== 0);
        firstName.setMessage(firstName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
        setShowSave(true);
    }, [firstName.value]);

    useEffect(() => {
        lastName.setValid(lastName.value.length !== 0);
        lastName.setMessage(lastName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
        setShowSave(true);
    }, [lastName.value]);

    useEffect(() => {
        place.setValid(place.value.length !== 0);
        place.setMessage(place.value.length !== 0 ? "" : REQUIRED_MESSAGE);
        setShowSave(true);
    }, [place.value]);

    useEffect(() => {
        address.setValid(address.value.length !== 0);
        address.setMessage(address.value.length !== 0 ? "" : REQUIRED_MESSAGE);
        setShowSave(true);
    }, [address.value]);

    useEffect(() => {
        postalCode.setValid(true);
        let valid = /^[1-9]\d{4}$/.test(postalCode.value);
        postalCode.setValid(valid);
        if (postalCode.value.length === 0) postalCode.setMessage(REQUIRED_MESSAGE);
        else postalCode.setMessage(valid ? WRONG_POSTAL_CODE : "");
        setShowSave(true);
    }, [postalCode.value]);

    useEffect(() => {
        userOib.setValid(true);
        if (/^\d{11}$/.test(userOib.value)) {
            if (userOib.value === props.user.oib) {
                userOib.setValid(true);
            } else {
                UserDataService
                    .isOibAvailable(userOib.value)
                    .then(response => {
                        userOib.setValid(response.data);
                        userOib.setMessage(response.data ? "" : OIB_UNAVAILABLE);
                    })
                    .catch(error => console.log(error.toString()));
            }
        } else {
            userOib.setValid(false);
            userOib.setMessage(userOib.value.length === 0 ? REQUIRED_MESSAGE : WRONG_OIB_MESSAGE);
        }
        setShowSave(true);
    }, [userOib.value]);

    useEffect(() => {
        companyOib.setValid(true);
        if (/^\d{11}$/.test(companyOib.value)) {
            if (companyOib.value === props.company.oib) {
                companyOib.setValid(true);
            } else {
                UserDataService
                    .isOibAvailable(companyOib.value)
                    .then(response => {
                        companyOib.setValid(response.data);
                        companyOib.setMessage(response.data ? "" : OIB_UNAVAILABLE);
                    })
                    .catch(error => console.log(error.toString()));
            }
        } else {
            companyOib.setValid(false);
            companyOib.setMessage(companyOib.value.length === 0 ? REQUIRED_MESSAGE : WRONG_OIB_MESSAGE);
        }
        setShowSave(true);
    }, [companyOib.value]);

    useEffect(() => {
        username.setValid(true);
        if (username.value.length === 0) {
            username.setValid(false);
            username.setMessage(REQUIRED_MESSAGE);
        } else if (username.value.includes("@")) {
            username.setValid(false);
            username.setMessage(WRONG_USERNAME);
        } else if (username.value === props.user.username) {
            username.setValid(true);
            username.setMessage("");
        } else {
            UserDataService
                .isUsernameAvailable(username.value)
                .then(response => {
                    username.setValid(response.data);
                    username.setMessage(response.data ? "" : USERNAME_UNAVAILABLE)
                })
                .catch(error => console.log(error.toString()));
        }
        setShowSave(true);
    }, [username.value]);

    useEffect(() => {
        companyName.setValid(true);
        if (companyName.value.length === 0) {
            companyName.setValid(false);
            companyName.setMessage(REQUIRED_MESSAGE);
        } else if (companyName.value.includes("@")) {
            companyName.setValid(false);
            companyName.setMessage(WRONG_COMPANY_NAME);
        } else if (companyName.value === props.company.companyName) {
            companyName.setValid(true);
            companyName.setMessage("");
        } else {
            UserDataService
                .isUsernameAvailable(companyName.value)
                .then(response => {
                    companyName.setValid(response.data);
                    companyName.setMessage(response.data ? "" : COMPANY_NAME_UNAVAILABLE);
                })
                .catch(error => console.log(error));
        }
        setShowSave(true);
    }, [companyName.value]);

    useEffect(() => {
        email.setValid(true);
        if (/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.value)) {
            if (email.value === props.user.email) {
                email.setValid(true);
            } else {
                UserDataService
                    .isEmailAvailable(email.value)
                    .then(response => {
                        email.setValid(response.data);
                        email.setMessage(response.data ? "" : EMAIL_UNAVAILABLE);
                    })
                    .catch(error => console.log(error.toString()));
            }
        } else {
            email.setValid(false);
            email.setMessage(email.value.length === 0 ? REQUIRED_MESSAGE : WRONG_EMAIL_MESSAGE)
        }
        setShowSave(true);
    }, [email.value]);

    useEffect(() => setShowSave(false), []);

    function goBack() {
        if (showSave) {
            alert("Changes you made will not be saved.");
        }
        props.goBack();
    }

    function isValid() {
        if (!firstName.isValid && !lastName.isValid && !email.isValid && !userOib.isValid) return false;

        if (isCompany) {
            return companyName.isValid && place.isValid && postalCode.isValid && address.isValid && companyOib.isValid;
        }

        return username.isValid;
    }

    function setAllChanged() {
        firstName.setChanged(true);
        lastName.setChanged(true);
        username.setChanged(true);
        companyName.setChanged(true);
        userOib.setChanged(true);
        companyOib.setChanged(true);
        place.setChanged(true);
        postalCode.setChanged(true);
        address.setChanged(true);
        email.setChanged(true);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        if (isCompany) {
            onSubmitCompany()
        } else {
            onSubmitUser()
        }
    }

    function onSubmitUser() {
        let user = JSON.parse(JSON.stringify(props.user));

        user.firstName = firstName.value;
        user.lastName = lastName.value;
        user.username = username.value;
        user.oib = userOib.value;
        user.email = email.value;

        updateUser(user, () => {
            localStorage.setItem(LOCAL_ATTRIBUTE_USERNAME, username.value);
            props.setUser(user);
            // props.goBack()
            window.location.reload(false);
        });
    }

    function onSubmitCompany() {
        let company = JSON.parse(JSON.stringify(props.company));

        company.location.place = {
            placeName: place.value,
            postalCode: postalCode.value
        };
        company.location.address = address.value;
        company.companyName = companyName.value;
        company.oib = companyOib.value;
        company.email = email.value;

        let user = JSON.parse(JSON.stringify(props.user));

        user.firstName = firstName.value;
        user.lastName = lastName.value;
        user.username = companyName.value;
        user.oib = userOib.value;
        user.email = email.value;

        updateUser(user, () => {
            props.setUser(user);
        });
        updateCompany(company, () => {
            localStorage.setItem(LOCAL_ATTRIBUTE_USERNAME, companyName.value);
            props.setCompany(company);
            // props.goBack();
            window.location.reload(false);
        });
    }

    function updateUser(u, afterUpdate) {
        console.log("updating user " + u.username);
        console.log(u);
        UserDataService
            .updateUser(u)
            .then(() => {
                console.log("user updated");
                if (afterUpdate) afterUpdate()
            })
            .catch(error => {
                console.log(error.toString());
                alert(error.toString());
            });
    }


    function updateCompany(c, afterUpdate) {
        AutoservisDataService
            .updateAutoservis(c)
            .then(() => {
                console.log("company updated");
                if (afterUpdate) afterUpdate()
            })
            .catch(error => {
                console.log(error.toString());
                alert(error.toString());
            });
    }

    function confirmDelete() {
        setDeleteConfirm(false);

        let username = AuthenticationService.getLoggedInUserName();
        UserDataService.deleteUser(username)
            .then(r => r.status)
            .catch(error => alert(error.toString()));
        props.logout();
    }

    function userForm() {
        return (
            <div>
                <div className="subTitle"><label><b>Podaci o korisniku</b></label></div>

                <FormInputRow label="Ime"
                              placeholder="Unesite ime" id="fname"
                              type="text" entity={firstName}/>
                <FormInputRow label="Prezime"
                              placeholder="Unesite prezime" id="lname"
                              type="text" entity={lastName}/>
                <FormLazyInputRow label="OIB"
                                  placeholder="Unesite OIB" id="oib"
                                  type="text" entity={userOib}/>
                <FormLazyInputRow label="Email"
                                  placeholder="Unesite email" id="email"
                                  type="text" entity={email}/>
            </div>
        )
    }

    function usernameForm() {
        return (
            <FormLazyInputRow label="Korisničko ime"
                              placeholder="Unesite korisničko ime" id="uname"
                              type="text" entity={username}/>
        )
    }

    function companyForm() {
        return (
            <div>
                <div className="subTitle"><label><b>Podaci o autoservisu</b></label></div>

                <FormLazyInputRow label="Naziv autoservisa (ujedno i korisničko ime admina)"
                                  placeholder="Unesite naziv autoservisa" id="cname"
                                  type="text" entity={companyName}/>
                <FormLazyInputRow label="OIB autoservisa"
                                  placeholder="Unesite OIB" id="coib"
                                  type="text" entity={companyOib}/>
                <FormInputRow label="Grad"
                              placeholder="Unesite grad" id="city"
                              type="text" entity={place}/>
                <FormInputRow label="Poštanski broj"
                              placeholder="Unesite poštanski broj" id="postalCode"
                              type="text" entity={postalCode}/>
                <FormInputRow label="Ulica i kućni broj"
                              placeholder="Unesite ulicu i kućni broj" id="adress"
                              type="text" entity={address}/>
            </div>
        )
    }

    return (
        <div className="dataChange">
            {userForm()}
            {isCompany ?
                companyForm() :
                usernameForm()
            }
            {showSave &&
            <button className="LoginButton" onClick={onSubmit}>Spremi promjene</button>
            }
            <PasswordChangeForm user={props.user}/>
            <button className="LoginButton" onClick={() => setDeleteConfirm(true)}>Obriši korisnički račun</button>
            {deleteConfirm &&
            <DeleteConfirmation confirmDelete={confirmDelete} closePopup={() => setDeleteConfirm(false)}
                                message="Želite li stvarno obrisati korisnički račun?"/>}
            <button className="cancelbtn2" onClick={goBack}>Poništi</button>
        </div>

    );
}

function PasswordChangeForm(props) {
    const [changePassword, setChangePassword] = useState(false);
    const [showSavePassword, setShowSavePassword] = useState(false);
    const oldPassword = useFormInput('');
    const password = useFormInput('');
    const retypedPassword = useFormInput('');

    useEffect(() => {
        let valid = oldPassword.value.length !== 0;
        oldPassword.setValid(valid);
        oldPassword.setMessage(valid ? "" : REQUIRED_MESSAGE);
        setShowSavePassword(valid);
    }, [oldPassword.value]);

    useEffect(() => {
        let valid = password.value.length >= 6;
        password.setValid(valid);
        password.setMessage(valid ? "" : PASSWORD_TOO_SHORT);
        setShowSavePassword(true);
    }, [password.value]);

    useEffect(() => {
        let valid = retypedPassword.value === password.value;
        retypedPassword.setValid(valid);
        retypedPassword.setMessage(valid ? "" : PASSWORD_MISMATCH);
        setShowSavePassword(true);
    }, [retypedPassword.value, password.value]);

    function onSubmit(e) {
        e.preventDefault();

        if (!oldPassword.isValid || !password.isValid || !retypedPassword.isValid) {
            oldPassword.setChanged(true);
            password.setChanged(true);
            retypedPassword.setChanged(true);
            return;
        }

        if (oldPassword.value === password.value) setChangePassword(false);

        let user = JSON.parse(JSON.stringify(props.user));
        user.password = oldPassword.value;

        UserDataService.checkPassword(user.username, user)
            .then(() => {
                user.password = password.value;
                console.log(user);
                UserDataService
                    .updateUserPassword(user.id, user)
                    .then(() => {
                        setChangePassword(false);
                    })
                    .catch(error => console.log(error.toString()));
            })
            .catch(() => {
                oldPassword.setValid(false);
                oldPassword.setMessage(WRONG_OLD_PASSWORD)
            });
    }

    function onCancel() {
        setChangePassword(false);
        oldPassword.clearValue();
        oldPassword.setChanged(false);
        password.clearValue();
        password.setChanged(false);
        retypedPassword.clearValue();
        retypedPassword.setChanged(false);
    }

    return (
        <div>
            {changePassword ?
                <form onSubmit={onSubmit}>
                    <div className="subTitle"><label><b>Promjena lozinke</b></label></div>

                    <FormLazyInputRow label="Stara lozinka"
                                      placeholder="Unesite staru lozinku" id="opsw"
                                      type="password" entity={oldPassword}/>
                    <FormLazyInputRow label="Nova lozinka"
                                      placeholder="Unesite novu lozinku (najmanje 6 znakova)" id="psw"
                                      type="password" entity={password}/>
                    <FormLazyInputRow label="Potvrda lozinke"
                                      placeholder="Ponovno unesite lozinku" id="rpsw"
                                      type="password" entity={retypedPassword}/>
                    {showSavePassword &&
                    <div>
                        <button className="LoginButton" type="submit">Potvrdi</button>
                        <button className="cancelbtn2" onClick={onCancel}>Odustani</button>
                    </div>
                    }
                    <div className="subTitle"><label/></div>
                </form> :
                <button className="LoginButton" onClick={() => setChangePassword(v => !v)}>Promjena lozinke</button>
            }
        </div>
    )
}