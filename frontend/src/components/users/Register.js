import React, {useEffect, useState} from 'react';
import '../../App.css';
import avatar from "../../assets/iconfinder_7_avatar_2754582.svg";
import AuthenticationService from "../../service/AuthenticationService";
import UserDataService from "../../service/UserDataService";
import AutoservisDataService from "../../service/AutoservisDataService";
import FormLazyInputRow from "../form_utils/FormLazyInputRow";
import FormInputRow from "../form_utils/FormInputRow";
import {
    COMPANY_NAME_UNAVAILABLE,
    EMAIL_UNAVAILABLE,
    OIB_UNAVAILABLE,
    PASSWORD_MISMATCH,
    PASSWORD_TOO_SHORT,
    REQUIRED_MESSAGE,
    USERNAME_UNAVAILABLE,
    WRONG_COMPANY_NAME,
    WRONG_EMAIL_MESSAGE,
    WRONG_OIB_MESSAGE,
    WRONG_POSTAL_CODE,
    WRONG_USERNAME
} from "../../Constants";
import {useFormInput} from "../form_utils/FormUtils";

export default function Register(props) {
    const firstName = useFormInput('');
    const lastName = useFormInput('');
    const userOib = useFormInput('');
    const username = useFormInput('');

    const companyName = useFormInput('');
    const companyOib = useFormInput('');
    const place = useFormInput('');
    const postalCode = useFormInput('');
    const address = useFormInput('');

    const email = useFormInput('');
    const password = useFormInput('');
    const retypedPassword = useFormInput('');

    const [personRegisterInfo, setPersonRegisterInfo] = useState(true);

    useEffect(() => {
        firstName.setValid(firstName.value.length !== 0);
        firstName.setMessage(firstName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [firstName.value]);

    useEffect(() => {
        lastName.setValid(lastName.value.length !== 0);
        lastName.setMessage(lastName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [lastName.value]);

    useEffect(() => {
        place.setValid(place.value.length !== 0);
        place.setMessage(place.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [place.value]);

    useEffect(() => {
        address.setValid(address.value.length !== 0);
        address.setMessage(address.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [address.value]);

    useEffect(() => {
        let valid = /^[1-9]\d{4}$/.test(postalCode.value);
        postalCode.setValid(valid);
        if (postalCode.value.length === 0) postalCode.setMessage(REQUIRED_MESSAGE);
        else postalCode.setMessage(valid ? WRONG_POSTAL_CODE : "");
    }, [postalCode.value]);

    useEffect(() => {
        userOib.setValid(true);
        if (/^\d{11}$/.test(userOib.value)) {
            UserDataService
                .isOibAvailable(userOib.value)
                .then(response => {
                    userOib.setValid(response.data);
                    userOib.setMessage(response.data ? "" : OIB_UNAVAILABLE);
                })
                .catch(error => console.log(error.toString()));
        } else {
            userOib.setValid(false);
            userOib.setMessage(userOib.value.length === 0 ? REQUIRED_MESSAGE : WRONG_OIB_MESSAGE);
        }
    }, [userOib.value]);

    useEffect(() => {
        companyOib.setValid(true);
        if (/^\d{11}$/.test(companyOib.value)) {
            UserDataService
                .isOibAvailable(companyOib.value)
                .then(response => {
                    companyOib.setValid(response.data);
                    companyOib.setMessage(response.data ? "" : OIB_UNAVAILABLE);
                })
                .catch(error => console.log(error.toString()));
        } else {
            companyOib.setValid(false);
            companyOib.setMessage(companyOib.value.length === 0 ? REQUIRED_MESSAGE : WRONG_OIB_MESSAGE);
        }
    }, [companyOib.value]);

    useEffect(() => {
        username.setValid(true);
        if (username.value.length === 0) {
            username.setValid(false);
            username.setMessage(REQUIRED_MESSAGE);
        } else if (username.value.includes("@")) {
            username.setValid(false);
            username.setMessage(WRONG_USERNAME);
        } else {
            UserDataService
                .isUsernameAvailable(username.value)
                .then(response => {
                    username.setValid(response.data);
                    username.setMessage(response.data ? "" : USERNAME_UNAVAILABLE)
                })
                .catch(error => console.log(error.toString()));
        }
    }, [username.value]);

    useEffect(() => {
        companyName.setValid(true);
        if (companyName.value.length === 0) {
            companyName.setValid(false);
            companyName.setMessage(REQUIRED_MESSAGE);
        } else if (companyName.value.includes("@")) {
            companyName.setValid(false);
            companyName.setMessage(WRONG_COMPANY_NAME);
        } else {
            UserDataService
                .isUsernameAvailable(companyName.value)
                .then(response => {
                    companyName.setValid(response.data);
                    companyName.setMessage(response.data ? "" : COMPANY_NAME_UNAVAILABLE);
                })
                .catch(error => console.log(error));
        }
    }, [companyName.value]);

    useEffect(() => {
        email.setValid(true);
        if (/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.value)) {
            UserDataService
                .isEmailAvailable(email.value)
                .then(response => {
                    email.setValid(response.data);
                    email.setMessage(response.data ? "" : EMAIL_UNAVAILABLE);
                })
                .catch(error => console.log(error.toString()));
        } else {
            email.setValid(false);
            email.setMessage(email.value.length === 0 ? REQUIRED_MESSAGE : WRONG_EMAIL_MESSAGE)
        }
    }, [email.value]);

    useEffect(() => {
        let valid = password.value.length >= 6;
        password.setValid(valid);
        password.setMessage(valid ? "" : PASSWORD_TOO_SHORT);
    }, [password.value]);

    useEffect(() => {
        let valid = (retypedPassword.value === password.value);
        retypedPassword.setValid(valid);
        retypedPassword.setMessage(valid ? "" : PASSWORD_MISMATCH);
    }, [retypedPassword.value, password.value]);

    function isValid() {
        if (!firstName.isValid || !lastName.isValid || !email.isValid ||
            !userOib.isValid || !password.isValid || !retypedPassword.isValid) return false;

        if (!personRegisterInfo) {
            return companyName.isValid && place.isValid && postalCode.isValid && address.isValid && companyOib.isValid;
        }

        return username.isValid;
    }

    function setAllChanged() {
        firstName.setChanged(true);
        lastName.setChanged(true);
        username.setChanged(true);
        companyName.setChanged(true);
        userOib.setChanged(true);
        companyOib.setChanged(true);
        place.setChanged(true);
        postalCode.setChanged(true);
        address.setChanged(true);
        email.setChanged(true);
        password.setChanged(true);
        retypedPassword.setChanged(true);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        if (!personRegisterInfo) {
            onSubmitCompany()
        } else {
            onSubmitUser()
        }
    }

    function onSubmitUser() {
        let user = {
            firstName: firstName.value,
            lastName: lastName.value,
            username: username.value,
            oib: userOib.value,
            email: email.value,
            password: password.value
        };

        UserDataService
            .createUser(user)
            .then(
                () => AuthenticationService
                    .executeJwtAuthenticationService(user.username, user.password)
                    .then((response) => {
                            AuthenticationService.registerSuccessfulLoginForJwt(user.username, response.data.token);
                            // props.history.push('/')
                            props.onLogin();
                            props.closePopup();
                        }
                    )
                    .catch(reason => alert(reason.toLocaleString()))
            )
            .catch(error => {
                console.log(error.toString());
                return alert("username/email already exists!");
            })
    }

    function onSubmitCompany() {
        const company = {
            location: {
                place: {
                    placeName: place.value,
                    postalCode: postalCode.value
                },
                address: address.value,
            },
            companyName: companyName.value,
            oib: companyOib.value,
            email: email.value
        };
        let user = {
            firstName: firstName.value,
            lastName: lastName.value,
            username: companyName.value,
            oib: userOib.value,
            email: email.value,
            password: password.value
        };

        AutoservisDataService
            .createAutoservis(company, user)
            .then(
                () => AuthenticationService
                    .executeJwtAuthenticationService(user.username, user.password)
                    .then((response) => {
                            AuthenticationService.registerSuccessfulLoginForJwt(user.username, response.data.token);
                            // props.history.push('/')
                            props.onLogin();
                            props.closePopup();
                        }
                    )
                    .catch(error => {
                        console.log(error.toString());
                        return alert("cannot login automatically but has created autoservis and user");
                    })
            )
            .catch(error => {
                console.log(error.toString());
                return alert("cannot register autoservis and user");
            })
    }

    function userRegisterForm() {
        return (
            <div>
                <div className="subTitle"><label><b>Podaci o korisniku</b></label></div>

                <FormInputRow label="Ime"
                              placeholder="Unesite ime" id="fname"
                              type="text" entity={firstName}/>
                <FormInputRow label="Prezime"
                              placeholder="Unesite prezime" id="lname"
                              type="text" entity={lastName}/>
                <FormLazyInputRow label="OIB"
                                  placeholder="Unesite OIB" id="oib"
                                  type="text" entity={userOib}/>
            </div>
        )
    }

    function usernameForm() {
        return (
            <FormLazyInputRow label="Korisničko ime"
                              placeholder="Unesite korisničko ime" id="uname"
                              type="text" entity={username}/>
        )
    }

    function companyRegisterForm() {
        return (
            <div>
                <div className="subTitle"><label><b>Podaci o autoservisu</b></label></div>

                <FormLazyInputRow label="Naziv autoservisa (ujedno i korisničko ime admina)"
                                  placeholder="Unesite naziv autoservisa" id="cname"
                                  type="text" entity={companyName}/>

                <FormLazyInputRow label="OIB autoservisa"
                                  placeholder="Unesite OIB" id="coib"
                                  type="text" entity={companyOib}/>

                <FormInputRow label="Grad"
                              placeholder="Unesite grad" id="city"
                              type="text" entity={place}/>

                <FormInputRow label="Poštanski broj"
                              placeholder="Unesite poštanski broj" id="postalCode"
                              type="text" entity={postalCode}/>

                <FormInputRow label="Ulica i kućni broj"
                              placeholder="Unesite ulicu i kućni broj" id="adress"
                              type="text" entity={address}/>
            </div>
        )
    }

    function loginRegisterForm() {
        return (
            <div>
                <div className="subTitle"><label><b>Podaci za prijavu</b></label></div>

                <FormLazyInputRow label="Email"
                                  placeholder="Unesite email" id="email"
                                  type="text" entity={email}/>

                <FormLazyInputRow label="Lozinka"
                                  placeholder="Unesite lozinku (najmanje 6 znakova)" id="psw"
                                  type="password" entity={password}/>

                <FormLazyInputRow label="Potvrda lozinke"
                                  placeholder="Ponovno unesite lozinku" id="rpsw"
                                  type="password" entity={retypedPassword}/>
            </div>
        )
    }

    function toggleRegisterInfo() {
        moveBackground();
        setPersonRegisterInfo(!personRegisterInfo);
    }

    function moveBackground() {
        const btn = document.getElementById('btn');
        btn.style.left = !personRegisterInfo ? "0px" : "105px"
    }

    return (
        <div className="modal ">
            <div className='modal-content'>

                <div className="imgcontainer">
                    <img src={avatar} alt="Avatar" className="avatar"/>
                </div>
                <div className="switchButtonBox">

                    <div id="btn" className="btn"/>
                    <button type="button" className="switchButton"
                            onClick={!personRegisterInfo ? toggleRegisterInfo : undefined}>Osoba
                    </button>
                    <button type="button" className="switchButton"
                            onClick={personRegisterInfo ? toggleRegisterInfo : undefined}>Autoservis
                    </button>
                </div>

                <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                <form onSubmit={onSubmit}>

                    <div className="register_container">

                        {userRegisterForm()}

                        {personRegisterInfo ?
                            usernameForm() :
                            companyRegisterForm()
                        }

                        {loginRegisterForm()}

                        <button type="submit" className="RegisterButton">Registracija</button>
                    </div>
                </form>

                <div>
                    <button type="button" className="cancelbtn2" onClick={props.closePopup}>Odustani</button>
                </div>
            </div>
        </div>
    );
}
