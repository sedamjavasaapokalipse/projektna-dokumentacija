import React, {useState} from 'react';
import '../../App.css';
import VehicleDataService from "../../service/VehicleDataService";
import DeleteConfirmation from "../form_utils/DeleteConfirmation";
import CreateAndEditVehicleForm from "./CreateAndEditVehicleForm";

export default function VehicleListItem(props) {
    const [showButtons, setShowButtons] = useState(false);
    const [showEditVehicle, setShowEditVehicle] = useState(false);
    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);
    const vehicle = props.vehicle;

    function toggleEditVehicle() {
        setShowEditVehicle(show => !show)
    }

    function toggleShowButtons() {
        setShowButtons(show => !show);
    }

    function deleteVehicle() {
        VehicleDataService
            .deleteVehicle(vehicle.registrationMark)
            .then(() => props.updateList())
            .catch(() => console.log("Vozilo se ne može obrisati"));
    }

    return (
        <li>
            <button className="partFont" onClick={toggleShowButtons}>
                <b>{vehicle.brand} {vehicle.model}</b>

            </button>
            {showButtons &&
            <div>
                <br/>
                <b>Registarske oznake:</b> {vehicle.registrationMark}
                <br/>
                <button onClick={toggleEditVehicle}>Uredi</button>
                <button className="btnPart" onClick={() => setShowDeleteConfirm(true)}>Izbriši</button>
            </div>}
            <br/><br/>
            {showEditVehicle &&
            <CreateAndEditVehicleForm
                create={false}
                vehicle={vehicle}
                updateList={props.updateList}
                closePopup={toggleEditVehicle}
            />
            }
            {showDeleteConfirm &&
            <DeleteConfirmation confirmDelete={deleteVehicle} closePopup={() => setShowDeleteConfirm(false)}
                                message="Želite li stvarno obrisati vozilo?"/>
            }
        </li>
    )
}