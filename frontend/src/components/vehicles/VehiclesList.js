import React, {useEffect, useState} from 'react';
import '../../App.css';
import AuthenticationService from "../../service/AuthenticationService";
import VehicleDataService from "../../service/VehicleDataService";
import VehicleListItem from "./VehicleListItem";
import CreateAndEditVehicleForm from "./CreateAndEditVehicleForm";

export default function VehiclesList() {
    const [showAddVehicle, setShowAddVehicle] = useState(false);
    const [htmlList, setHtmlList] = useState(null);

    useEffect(() => updateList(), []);

    function toggleAddVehicle() {
        setShowAddVehicle(a => !a);
    }

    function updateList() {
        VehicleDataService
            .getVehicles(AuthenticationService.getLoggedInUserName())
            .then(r => {
                setHtmlList(r.data.map(v => {
                    return <VehicleListItem updateList={updateList} vehicle={v} key={v.id}/>;
                }));
            })
            .catch(error => console.log(error.toString()))
    }

    return (
        <div className="partsList">
            <h2>Moja vozila</h2>
            <ul>
                {htmlList}
            </ul>
            <button className="btnPart" onClick={toggleAddVehicle}>Dodaj vozilo</button>
            {showAddVehicle &&
            <CreateAndEditVehicleForm
                create={true}
                updateList={updateList}
                closePopup={toggleAddVehicle}
            />
            }
        </div>
    )
}