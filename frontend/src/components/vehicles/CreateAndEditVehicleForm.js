import React, {useEffect} from 'react';
import '../../App.css';
import icon from '../../assets/Layer_1.svg'
import AuthenticationService from "../../service/AuthenticationService";
import {useFormInput} from "../form_utils/FormUtils"
import VehicleDataService from "../../service/VehicleDataService";
import FormInputRow from "../form_utils/FormInputRow";
import {REGISTRATION_MARK_UNAVAILABLE, REQUIRED_MESSAGE} from "../../Constants";
import HuoDataService from "../../service/HuoDataService";
import {ILLEGAL_REGISTRATION_NUMBER} from "../../Constants";

export default function CreateAndEditVehicleForm(props) {
    const vehicle = props.vehicle;
    const brand = useFormInput(vehicle === undefined ? "" : vehicle.brand);
    const model = useFormInput(vehicle === undefined ? "" : vehicle.model);
    const registrationMark = useFormInput(vehicle === undefined ? "" : vehicle.registrationMark);

    useEffect(() => {
        brand.setValid(brand.value.length !== 0);
        brand.setMessage(brand.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [brand.value]);

    useEffect(() => {
        model.setValid(model.value.length !== 0);
        model.setMessage(model.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [model.value]);

    useEffect(() => {
        registrationMark.setValid(true);
        if (/^(BJ|BM|ČK|DA|DE|DJ|DU|GS|IM|KA|KC|KR|KT|KŽ|MA|NA|NG|OG|OS|PU|PŽ|RI|SB|SK|SL|PS|ST|ŠI|VK|VT|VU|VŽ|ZD|ZG|ŽU)\d{3,4}[A-Z]{1,2}$/
            .test(registrationMark.value.toUpperCase())) {
            if (!props.create && vehicle.registrationMark.toUpperCase() === registrationMark.value.toUpperCase()) return;
            VehicleDataService
                .isRegistrationMarkAvailable(registrationMark.value)
                .then(r => {
                    registrationMark.setValid(r.data);
                    registrationMark.setMessage(r.data ? "" : REGISTRATION_MARK_UNAVAILABLE)
                })
                .catch(error => console.log(error.toString()));

            HuoDataService.existsInHuoRegister(registrationMark.value)
                .then(r => {
                    registrationMark.setValid(r.data);
                    registrationMark.setMessage(r.data ? "" : ILLEGAL_REGISTRATION_NUMBER);
                });
        } else {
            registrationMark.setValid(false);
        }
    }, [registrationMark.value]);

    function isValid() {
        return brand.isValid && model.isValid && registrationMark.isValid;
    }

    function setAllChanged() {
        brand.setChanged(true);
        model.setChanged(true);
        registrationMark.setChanged(true);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        const newVehicle = vehicle ? JSON.parse(JSON.stringify(vehicle)) : {};
        newVehicle.brand = brand.value;
        newVehicle.model = model.value;
        newVehicle.registrationMark = registrationMark.value;

        if (props.create) {
            VehicleDataService
                .createVehicle(newVehicle, AuthenticationService.getLoggedInUserName())
                .then(() => {
                    props.updateList();
                    props.closePopup();
                })
                .catch(error => alert(error.toString()));
        } else {
            VehicleDataService
                .updateVehicle(newVehicle, newVehicle.id)
                .then(() => {
                    props.updateList();
                    props.closePopup();
                })
                .catch(error => alert(error.toString()));
        }
    }

    return (
        <div className='container'>
            <div className='modal'>
                <div className="modal-content" id="id01">
                    <div className="imgcontainer">
                        <img src={icon} alt="Alat" className="avatar"/>
                    </div>

                    <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                    <form onSubmit={onSubmit}>

                        <div className="login_container">
                            <FormInputRow label="Marka" type="text"
                                          placeholder="Unesite marku vozila"
                                          id="brand" entity={brand}/>

                            <FormInputRow label="Model" type="text"
                                          placeholder="Unesite model vozila"
                                          id="model" entity={model}/>

                            <FormInputRow label="Registracijska oznaka" type="text"
                                          placeholder="Unesite registracijsku oznaku vozila"
                                          id="reg" entity={registrationMark}/>
                        </div>
                        <button type="submit" className="SavePartButton">Spremi vozilo</button>
                    </form>

                    <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
                </div>
            </div>
        </div>
    )
}

