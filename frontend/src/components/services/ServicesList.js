import React, {useState, useEffect} from 'react';
import '../../App.css';
import CreateAndEditServicesForm from "./CreateAndEditServicesForm";
import AuthenticationService from "../../service/AuthenticationService";
import ServiceDataService from "../../service/ServiceDataService";
import ServicesListItem from "./ServicesListItem";

export default function ServicesList(props) {
    const [showAddService, setShowAddService] = useState(false);
    const [htmlList, setHtmlList] = useState(null);

    useEffect(() => updateList(), []);

    function toggleAddService() {
        setShowAddService(show => !show)
    }

    function updateList() {
        ServiceDataService
            .listServices(AuthenticationService.getLoggedInUserName())
            .then(r => {
                setHtmlList(
                    r.data.map((service) => <ServicesListItem key={service.id} service={service}
                                                                 updateList={updateList}/>)
                );
            })
            .catch(() => console.log("Error while updating services!"));
    }

    return (
        <div className="partsList">
            <h2>Cjenik usluga autoservisa</h2>
            <ul>
                {htmlList}
            </ul>
            {props.role === "ADMIN" &&
                <button className="btnPart" onClick={toggleAddService}>Dodaj novu uslugu</button>
            }

            {showAddService &&
            <CreateAndEditServicesForm
                create={true}
                updateList={updateList}
                closePopup={toggleAddService}
            />
            }
        </div>
    )
}