import React, {useEffect} from 'react';
import '../../App.css';
import icon from '../../assets/Layer_1.svg'
import ServiceDataService from "../../service/ServiceDataService";
import AuthenticationService from "../../service/AuthenticationService";
import {useFormInput} from "../form_utils/FormUtils";
import FormInputRow from "../form_utils/FormInputRow";
import {ILLEGAL_NUMBER_MESSAGE, REQUIRED_MESSAGE} from "../../Constants";

export default function CreateAndEditServicesForm(props) {
    const service = props.service;
    const serviceName = useFormInput(service === undefined ? "" : service.serviceName);
    const price = useFormInput(service === undefined ? "" : service.price);
    const description = useFormInput(service === undefined ? "" : service.description);

    useEffect(() => {
        serviceName.setValid(serviceName.value.length !== 0);
        serviceName.setMessage(serviceName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [serviceName.value]);

    useEffect(() => {
        let valid = price.value.length !== 0 && !isNaN(price.value) && price.value > 0;
        price.setValid(valid);
        if (price.value.length === 0) price.setMessage(REQUIRED_MESSAGE);
        else price.setMessage(valid ? "" : ILLEGAL_NUMBER_MESSAGE);
    }, [price.value]);

    useEffect(() => {
        description.setValid(true);
        description.setMessage("");
    }, [description.value]);

    function onSubmit(event) {
        event.preventDefault();
        const service = {
            serviceName: serviceName.value,
            price: price.value,
            description: description.value
        };

        if (props.create) {
            ServiceDataService
                .createService(AuthenticationService.getLoggedInUserName(), service)
                .then(() => {
                    props.closePopup();
                    props.updateList();
                })
                .catch(() => console.log("Error while adding service!"));
        } else {
            ServiceDataService
                .updateService(props.service.id, service)
                .then(() => {
                    props.closePopup();
                    props.updateList();
                })
                .catch(() => console.log("Error while updating service!"));
        }

    }

    return (
        <div className='container'>
            <div className='modal'>
                <div className="modal-content" id="id01">
                    <div className="imgcontainer">
                        <img src={icon} alt="Alat" className="avatar"/>
                    </div>

                    <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                    <form onSubmit={onSubmit}>

                        <div className="login_container">
                            <FormInputRow label="Naziv usluge" type="text"
                                          placeholder="Unesite naziv usluge"
                                          id="name" entity={serviceName}/>

                            <FormInputRow label="Cijena (kn)" type="number" step="0.01"
                                          placeholder="Unesite cijenu usluge (u kunama)"
                                          id="price" entity={price}/>

                            <FormInputRow label="Opis" type="text"
                                          placeholder="Unesite opis usluge"
                                          id="description" entity={description}/>
                        </div>
                        <button type="submit" className="SavePartButton">Spremi uslugu</button>
                    </form>

                    <button type="button" onClick={props.closePopup} className="cancelbtn2">Odustani</button>
                </div>
            </div>
        </div>
    )
}

