import React, {useState} from 'react';
import ServiceDataService from "../../service/ServiceDataService";
import CreateAndEditServicesForm from "./CreateAndEditServicesForm";
import DeleteConfirmation from "../form_utils/DeleteConfirmation";


export default function ServicesListItem(props) {

    const [showEditService, setShowEditService] = useState(false);
    const [showButtons, setShowButtons] = useState(false);
    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);
    const service = props.service;

    function toggleEditService() {
        setShowEditService(show => !show)
    }

    function toggleShowButtons() {
        setShowButtons(show => !show);
    }

    function deleteService() {
        ServiceDataService.deleteService(service.id)
            .then(() => props.updateList())
            .catch(() => console.log("Error while updating part!"));
    }

    return (
        <li>
            <div>
                <button className="partFont" onClick={toggleShowButtons}>
                    <b>{service.serviceName}</b>

                </button>
            </div>
            {showButtons &&
            <div>
                <br/>
                <b>Cijena:</b> {service.price} kn
                <br/>
                <b>Opis:</b> {service.description.length !== 0 ? service.description : "-"}
                <br/>
                <button onClick={toggleEditService}>Uredi {service.serviceName}</button>
                <button className="btnPart" onClick={() => setShowDeleteConfirm(true)}>Izbriši {service.serviceName}</button>
            </div>}
            <br/><br/>
            {showEditService &&
            <CreateAndEditServicesForm
                create={false}
                service={service}
                updateList={props.updateList}
                closePopup={toggleEditService}
            />
            }
            {showDeleteConfirm &&
            <DeleteConfirmation confirmDelete={deleteService} closePopup={() => setShowDeleteConfirm(false)}
                                message="Želite li stvarno obrisati odabranu uslugu?"/>
            }
        </li>
    )
}