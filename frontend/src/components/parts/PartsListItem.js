import React, {useState} from 'react';
import PartDataService from "../../service/PartDataService";
import CreateAndEditPartForm from "./CreateAndEditPartForm";
import DeleteConfirmation from "../form_utils/DeleteConfirmation";

export default function PartsListItem(props) {
    const [showEditPart, setShowEditPart] = useState(false);
    const [showButtons, setShowButtons] = useState(false);
    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);
    const part = props.part;

    function toggleEditPart() {
        setShowEditPart(show => !show)
    }

    function toggleShowButtons() {
        setShowButtons(show => !show);
    }

    function deletePart() {
        PartDataService
            .deletePart(part.id)
            .then(() => props.updateList())
            .catch(() => console.log("Error while deleting part!"));
    }

    return (
        <li>
            <button className="partFont" onClick={toggleShowButtons}>
                <b>{part.partName}</b>
            </button>
            {showButtons &&
            <div>
                <br/>
                <b>Cijena:</b> {part.price} kn
                <br/>
                <b>Predviđena kilometraža:</b> {part.estimatedKilometers} km
                <br/>
                {props.role === "ADMIN" &&
                    <button onClick={toggleEditPart}>Uredi {part.partName}</button>
                }
                {props.role === "ADMIN" &&
                <button className="btnPart" onClick={() => setShowDeleteConfirm(true)}>Izbriši {part.partName}</button>
                }
            </div>}
            <br/><br/>
            {showEditPart &&
            <CreateAndEditPartForm
                create={false}
                part={part}
                updateList={props.updateList}
                closePopup={toggleEditPart}
            />
            }
            {showDeleteConfirm &&
            <DeleteConfirmation confirmDelete={deletePart} closePopup={() => setShowDeleteConfirm(false)}
                                message="Želite li stvarno obrisati dio?"/>
            }
        </li>
    )
}