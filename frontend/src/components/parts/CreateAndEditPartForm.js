import React, {useEffect} from 'react';
import '../../App.css';
import icon from '../../assets/Layer_1.svg'
import PartDataService from "../../service/PartDataService";
import AuthenticationService from "../../service/AuthenticationService";
import {useFormInput} from "../form_utils/FormUtils";
import FormInputRow from "../form_utils/FormInputRow";
import {ILLEGAL_NUMBER_MESSAGE, REQUIRED_MESSAGE} from "../../Constants";

export default function CreateAndEditPartForm(props) {
    const part = props.part;
    const partName = useFormInput(part === undefined ? "" : part.partName);
    const price = useFormInput(part === undefined ? "" : part.price);
    const estKm = useFormInput(part === undefined ? "" : part.estimatedKilometers);

    useEffect(() => {
        partName.setValid(partName.value.length !== 0);
        partName.setMessage(partName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [partName.value]);

    useEffect(() => {
        let valid = price.value.length !== 0 && !isNaN(price.value) && price.value > 0;
        price.setValid(valid);
        if (price.value.length === 0) price.setMessage(REQUIRED_MESSAGE);
        else price.setMessage(valid ? "" : ILLEGAL_NUMBER_MESSAGE);
    }, [price.value]);

    useEffect(() => {
        let valid = estKm.value.length !== 0 && !isNaN(estKm.value) && estKm.value > 0;
        estKm.setValid(valid);
        if (estKm.value.length === 0) estKm.setMessage(REQUIRED_MESSAGE);
        else estKm.setMessage(valid ? "" : ILLEGAL_NUMBER_MESSAGE);
    }, [estKm.value]);

    function isValid() {
        return partName.isValid && price.isValid && estKm.isValid;
    }

    function setAllChanged() {
        partName.setChanged(true);
        price.setChanged(true);
        estKm.setChanged(true);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        const part = {
            partName: partName.value,
            price: price.value,
            estimatedKilometers: estKm.value
        };

        if (props.create) {
            PartDataService
                .createPart(part, AuthenticationService.getLoggedInUserName())
                .then(() => {
                    props.closePopup();
                    props.updateList();
                })
                .catch(() => console.log("Error while adding part!"));
        } else {
            PartDataService
                .updatePart(props.part.id, part)
                .then(() => {
                    props.closePopup();
                    props.updateList();
                })
                .catch(() => console.log("Error while updating part!"));
        }
    }

    return (
        <div className='container'>
            <div className='modal'>
                <div className="modal-content" id="id01">
                    <div className="imgcontainer">
                        <img src={icon} alt="Alat" className="avatar"/>
                    </div>

                    <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                    <form onSubmit={onSubmit}>

                        <div className="login_container">
                            <FormInputRow label="Naziv dijela" type="text"
                                          placeholder="Unesite naziv automobilskog dijela"
                                          id="name" entity={partName}/>

                            <FormInputRow label="Cijena (kn)" type="number" step="0.01"
                                          placeholder="Unesite cijenu automobilskog dijela (u kunama)"
                                          id="price" entity={price}/>

                            <FormInputRow label="Predviđena kilometraža" type="number"
                                          placeholder="Unesite predviđenu kilometražu (rok trajanja)"
                                          id="estKm" entity={estKm}/>
                        </div>
                        <button type="submit" className="SavePartButton">Spremi automobilski dio</button>
                    </form>

                    <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
                </div>
            </div>
        </div>
    )
}

