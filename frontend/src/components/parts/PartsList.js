import React, {useEffect, useState} from 'react';
import '../../App.css';
import CreateAndEditPartForm from "./CreateAndEditPartForm";
import PartDataService from "../../service/PartDataService";
import AuthenticationService from "../../service/AuthenticationService";
import PartsListItem from "./PartsListItem";

export default function PartsList(props) {
    const [showAddPart, setShowAddPart] = useState(false);
    const [htmlList, setHtmlList] = useState(null);

    useEffect(() => updateList(), []);

    function toggleAddPart() {
        setShowAddPart(show => !show)
    }

    function updateList() {
        PartDataService
            .listAllByCompanyName(AuthenticationService.getLoggedInUserName())
            .then(r => {
                setHtmlList(
                    r.data.map((part) => <PartsListItem role={props.role} key={part.id} part={part} updateList={updateList}/>)
                );
            })
            .catch(() => console.log("Oopsie"));
    }

    return (
        <div className="partsList">
            <h2>Automobilski dijelovi</h2>
            <ul>
                {htmlList}
            </ul>
            {props.role === "ADMIN" &&
                <button className="btnPart" onClick={toggleAddPart}>Dodaj novi automobilski dio</button>
            }

            {showAddPart &&
            <CreateAndEditPartForm
                create={true}
                updateList={updateList}
                closePopup={toggleAddPart}
            />
            }
        </div>
    )
}