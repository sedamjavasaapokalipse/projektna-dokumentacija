import React, {useEffect} from 'react';
import '../../App.css';
import avatar from "../../assets/iconfinder_7_avatar_2754582.svg";
import AuthenticationService from "../../service/AuthenticationService";
import UserDataService from "../../service/UserDataService";
import FormInputRow from "../form_utils/FormInputRow";
import FormLazyInputRow from "../form_utils/FormLazyInputRow";
import {
    EMAIL_UNAVAILABLE,
    OIB_UNAVAILABLE,
    PASSWORD_MISMATCH,
    PASSWORD_TOO_SHORT,
    REQUIRED_MESSAGE,
    USERNAME_UNAVAILABLE,
    WRONG_EMAIL_MESSAGE,
    WRONG_OIB_MESSAGE,
    WRONG_USERNAME
} from "../../Constants";
import {useFormInput} from "../form_utils/FormUtils";

export default function RegisterMechanic(props) {
    const firstName = useFormInput('');
    const lastName = useFormInput('');
    const userOib = useFormInput('');
    const username = useFormInput('');

    const email = useFormInput('');
    const password = useFormInput('');
    const retypedPassword = useFormInput('');

    useEffect(() => {
        firstName.setValid(firstName.value.length !== 0);
        firstName.setMessage(firstName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [firstName.value]);

    useEffect(() => {
        lastName.setValid(lastName.value.length !== 0);
        lastName.setMessage(lastName.value.length !== 0 ? "" : REQUIRED_MESSAGE);
    }, [lastName.value]);

    useEffect(() => {
        if (/^\d{11}$/.test(userOib.value)) {
            UserDataService
                .isOibAvailable(userOib.value)
                .then(response => {
                    userOib.setValid(response.data);
                    userOib.setMessage(response.data ? "" : OIB_UNAVAILABLE);
                })
                .catch(error => console.log(error.toString()));
        } else {
            userOib.setValid(false);
            userOib.setMessage(userOib.value.length === 0 ? REQUIRED_MESSAGE : WRONG_OIB_MESSAGE);
        }
    }, [userOib.value]);

    useEffect(() => {
        if (username.value.length === 0) {
            username.setValid(false);
            username.setMessage(REQUIRED_MESSAGE);
        } else if (username.value.includes("@")) {
            username.setValid(false);
            username.setMessage(WRONG_USERNAME);
        } else {
            UserDataService
                .isUsernameAvailable(username.value)
                .then(response => {
                    username.setValid(response.data);
                    username.setMessage(response.data ? "" : USERNAME_UNAVAILABLE)
                })
                .catch(error => console.log(error.toString()));
        }
    }, [username.value]);

    useEffect(() => {
        if (/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.value)) {
            UserDataService
                .isEmailAvailable(email.value)
                .then(response => {
                    email.setValid(response.data);
                    email.setMessage(response.data ? "" : EMAIL_UNAVAILABLE);
                })
                .catch(error => console.log(error.toString()));
        } else {
            email.setValid(false);
            email.setMessage(email.value.length === 0 ? REQUIRED_MESSAGE : WRONG_EMAIL_MESSAGE)
        }
    }, [email.value]);

    useEffect(() => {
        let valid = password.value.length >= 6;
        password.setValid(valid);
        password.setMessage(valid ? "" : PASSWORD_TOO_SHORT);
    }, [password.value]);

    useEffect(() => {
        let valid = retypedPassword.value === password.value;
        retypedPassword.setValid(valid);
        retypedPassword.setMessage(valid ? "" : PASSWORD_MISMATCH);
    }, [retypedPassword.value, password.value]);

    function isValid() {
        if (!firstName.isValid && !lastName.isValid && !email.isValid &&
            !userOib.isValid && !password.isValid && !retypedPassword.isValid) return false;

        return username.isValid;
    }

    function setAllChanged() {
        firstName.setChanged(true);
        lastName.setChanged(true);
        username.setChanged(true);
        userOib.setChanged(true);
        email.setChanged(true);
        password.setChanged(true);
        retypedPassword.setChanged(true);
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        onSubmitUser();
    }

    function onSubmitUser() {
        let user = {
            firstName: firstName.value,
            lastName: lastName.value,
            username: username.value,
            oib: userOib.value,
            email: email.value,
            password: password.value
        };

        UserDataService
            .createMechanic(AuthenticationService.getLoggedInUserName(), user)
            .then(() => {
                    props.updateList();
                    props.closePopup();
                }
            )
            .catch(error => {
                console.log(error.toString());
                return alert("username/email already exists!");
            })
    }

    function userRegisterForm() {
        return (

            <div className="register-mechanic">
                <div className="subTitle"><label><b>Podaci o korisniku</b></label></div>

                <FormInputRow label="Ime"
                              placeholder="Unesite ime" id="fname"
                              type="text" entity={firstName}/>

                <FormInputRow label="Prezime"
                              placeholder="Unesite prezime" id="lname"
                              type="text" entity={lastName}/>

                <FormLazyInputRow label="OIB"
                                  placeholder="Unesite OIB" id="oib"
                                  type="text" entity={userOib}/>

                <FormLazyInputRow label="Korisničko ime"
                                  placeholder="Unesite korisničko ime" id="uname"
                                  type="text" entity={username}/>

                <div className="subTitle"><label><b>Podaci za prijavu</b></label></div>

                <FormLazyInputRow label="Email"
                                  placeholder="Unesite email" id="email"
                                  type="text" entity={email}/>

                <FormLazyInputRow label="Lozinka"
                                  placeholder="Unesite lozinku (najmanje 6 znakova)" id="psw"
                                  type="password" entity={password}/>

                <FormLazyInputRow label="Potvrda lozinke"
                                  placeholder="Ponovno unesite lozinku" id="rpsw"
                                  type="password" entity={retypedPassword}/>
            </div>
        )
    }

    return (
        <div className="modal ">
            <div className='modal-content'>

                <div className="imgcontainer">
                    <img src={avatar} alt="Avatar" className="avatar"/>
                </div>

                <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                <form onSubmit={onSubmit}>

                    <div className="register_container">

                        {userRegisterForm()}

                        <div className="register-mechanic">
                            <button type="submit" className="RegisterButton">Registracija</button>
                        </div>

                    </div>
                </form>

                <div className="register-mechanic">
                    <button type="button" className="cancelbtn2" onClick={props.closePopup}>Odustani</button>
                </div>
            </div>
        </div>
    );
}


