import React, {useState} from "react";
import UserDataService from "../../service/UserDataService";
import AuthenticationService from "../../service/AuthenticationService";
import DeleteConfirmation from "../form_utils/DeleteConfirmation";


export default function MechanicsListmechanic(props) {
    const [showButtons, setShowButtons] = useState(false);
    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);
    const mechanic = props.mechanic;

    function toggleShowButtons() {
        setShowButtons(show => !show);
    }

    function removeMechanic() {
        UserDataService
            .removeMechanic(AuthenticationService.getLoggedInUserName(), mechanic.username)
            .then(() => props.updateList())
            .catch(error => console.log(error));
    }

    return (
        <li>
            <button className="partFont" onClick={toggleShowButtons}>
                <b>{mechanic.firstName} {mechanic.lastName}</b>

            </button>
            {showButtons &&
                <div>
                    <br/>
                    <b>OIB: </b> {mechanic.oib}
                    <br/>
                    <button onClick={() => setShowDeleteConfirm(true)}>Ukloni s popisa automehaničara</button>
                </div>

            }
            <br/><br/>
            {showDeleteConfirm &&
            <DeleteConfirmation confirmDelete={removeMechanic}
                                closePopup={() => setShowDeleteConfirm(false)}
                                message="Želite li stvarno obrisati mehaničara? To će obrisati i njegov korisnički račun."/>
            }
        </li>
    )
}