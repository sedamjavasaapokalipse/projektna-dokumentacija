import React, {useEffect, useState} from 'react';
import '../../App.css';
import RegisterMechanic from "./RegisterMechanic";
import AutoservisDataService from "../../service/AutoservisDataService";
import AuthenticationService from "../../service/AuthenticationService";
import MechanicsListItem from "./MechanicsListItem";

export default function MechanicsList(props) {
    const [showRegisterMechanic, setShowRegisterMechanic] = useState(false);
    const [htmlList, setHtmlList] = useState(null);

    useEffect(() => updateList(), []);

    function toggleAddMechanic() {
        setShowRegisterMechanic(v => !v)
    }

    function updateList() {
        AutoservisDataService
            .getMechanics(AuthenticationService.getLoggedInUserName())
            .then(r => {
                setHtmlList(
                    r.data.map(m => <MechanicsListItem mechanic={m} updateList={updateList} key={m.id}/>)
                );
            })
            .catch(() => console.log("Oopsie"));
    }

    return (
        <div className="partsList">
            <h2>Automehaničari</h2>
            <ul>
                {htmlList}
            </ul>
            <button className="btnPart" onClick={toggleAddMechanic}>Dodaj novog automehaničara</button>
            {showRegisterMechanic &&
            <RegisterMechanic
                updateList={updateList}
                closePopup={toggleAddMechanic}
            />
            }
        </div>
    )
}