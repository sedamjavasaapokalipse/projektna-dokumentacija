import React, {useEffect, useState} from 'react';
import AuthenticationService from "../../service/AuthenticationService";
import OrderDataService from "../../service/OrderDataService";
import OrderListItem from "./OrderListItem";
import OrderForm from "./OrderForm";

export default function OrderList() {
    const [updateVehicleService, setUpdateVehicleService] = useState(false);
    const [allVehicleServices, setAllVehicleServices] = useState([]);
    const [chosenVehicleService, setChosenVehicleService] = useState(null);

    useEffect(() => {
        OrderDataService
            .retrieveVehicleServicesByMechanic(AuthenticationService.getLoggedInUserName())
            .then(response => {
                console.log(response.data);
                console.log("ovdje");
                setAllVehicleServices(response.data.map(vs => <OrderListItem toggleUpdateVehicleService={toggleUpdateVehicleService} vehicleService={vs} key={vs.serviceId}/>))
            })
            .catch(error => alert(error.toString()));
    }, []);

    function toggleUpdateVehicleService(vehicleService) {
        setUpdateVehicleService(a => !a);
        setChosenVehicleService(vehicleService)
    }

    return (
        <div className="partsList">
            <h2>Nalozi</h2>
            <ul>
                {allVehicleServices}
            </ul>
            {updateVehicleService &&
            <OrderForm
                closePopup={toggleUpdateVehicleService}
                vehicleService={chosenVehicleService}
            />
            }
        </div>
    )

}

