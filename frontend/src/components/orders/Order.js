import React, {useEffect, useState} from 'react';
import AuthenticationService from "../../service/AuthenticationService";
import VehicleDataService from "../../service/VehicleDataService";
import AutoservisDataService from "../../service/AutoservisDataService";
import OrderDataService from "../../service/OrderDataService";
import {useFormInput} from "../form_utils/FormUtils"
import "../../App.css"
export default function Order() {
    const [allVehicles, setAllVehicles] = useState([]);
    const [allCompanies, setAllCompanies] = useState([]);
    const [allOrders, setAllOrders] = useState([]);
    const companyId = useFormInput("");
    const registrationMark = useFormInput("");

    useEffect(() => {
        AutoservisDataService
            .retrieveAutoservisi()
            .then(response => {
                setAllCompanies(response.data)
            })
            .catch(error => alert(error.toString()));
    }, []);

    useEffect(() => {
        VehicleDataService
            .getVehicles(AuthenticationService.getLoggedInUserName())
            .then(response => {
                setAllVehicles(response.data)
            })
            .catch(error => alert(error.toString()));
    }, []);

    useEffect(() => {
        OrderDataService
            .retrieveVehicleServicesByUsername(AuthenticationService.getLoggedInUserName())
            .then(r => {
                console.log(r.data);
                setAllOrders(r.data);
            })
            .catch(error => console.log(error.toString()));
    }, []);


    function onSubmit(event) {
        event.preventDefault();

        if (companyId.value === "" || registrationMark.value === "") {
            return;
        }

        let vehicleService = {
            autoservis: null,
            vehicle: null
        };

        VehicleDataService
            .getVehicle(registrationMark.value)
            .then((response) => {
                vehicleService.vehicle = response.data;
                console.log(response.data);
                AutoservisDataService
                    .getCompanyById(parseInt(companyId.value))
                    .then((response) => {
                        vehicleService.autoservis = response.data;
                        console.log(vehicleService);
                        OrderDataService
                            .createVehicleService(vehicleService)
                            .then(() => {
                                let orders = allOrders.concat([vehicleService]);
                                setAllOrders(orders);
                                console.log("added");
                            })
                            .catch(error => alert(error.toString()));
                    })
                    .catch(error => alert(error.toString()));
            })
            .catch(error => alert(error.toString()));
    }

    return (
        <div className={"container"}>
            {allOrders.length !== 0 &&
            <div>
                <div className="subTitle"><label><b>Otvoreni nalozi</b></label></div>
                <ul>
                    {allOrders.map(order => {
                        return <li key={order.vehicle.registrationMark}>
                            <label>{order.vehicle.registrationMark}{/*, {order.autoservis.companyName}*/}</label>
                        </li>;
                    })}
                </ul>
                <br/><br/>
            </div>
            }
            <form onSubmit={onSubmit}>
                <label><b>Izaberi vozilo</b></label>
                <select className="vehicle" onChange={registrationMark.onChange}>
                    <option disabled selected value> -- izaberi vozilo --</option>
                    {allVehicles.map((v) => {
                        return <option key={v.registrationMark}
                                       value={v.registrationMark}>{v.registrationMark}</option>;
                    })}
                </select>
                <br/>
                <label><b>Izaberi autoservis</b></label>
                <select className="autoservis" onChange={companyId.onChange}>
                    <option disabled selected value> -- izaberi autoservis --</option>
                    {allCompanies.map((c) => {
                        return <option key={c.id} value={c.id}>{c.companyName}</option>;
                    })}
                </select>
                <br/>
                <button type="submit" className="SavePartButton">Otvori nalog</button>
            </form>

        </div>

    )
}