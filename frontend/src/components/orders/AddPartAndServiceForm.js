import React, {useEffect, useState} from 'react';
import '../../App.css';
import AuthenticationService from "../../service/AuthenticationService";
import PartDataService from "../../service/PartDataService";
import {useFormInput} from "../form_utils/FormUtils";
import ServiceDataService from "../../service/ServiceDataService";

export default function AddPartAndServiceForm(props) {
    const [allParts, setAllParts] = useState([]);
    const [allServices, setAllServices] = useState([]);
    const partId = useFormInput("");
    const serviceId = useFormInput("");
    const [part, setPart] = useState(null);
    const [service, setService] = useState(null);

    useEffect(() => {
        PartDataService
            .listAllByMechanic(AuthenticationService.getLoggedInUserName())
            .then((r) => {
                setAllParts(r.data);
            })
            .catch(error => console.log(error.toString()));
        ServiceDataService
            .listAllByMechanic(AuthenticationService.getLoggedInUserName())
            .then((r) => {
                setAllServices(r.data);
            })
            .catch(error => console.log(error.toString()))
    }, []);

    useEffect(() => {
        PartDataService
            .getPart(partId.value)
            .then(r => setPart(r.data))
            .catch(error => console.log(error.toString()));
    }, [partId.value]);

    useEffect(() => {
        ServiceDataService
            .getService(serviceId.value)
            .then(r => setService(r.data))
            .catch(error => console.log(error.toString()));
    }, [serviceId.value]);

    function onSubmit(e) {
        e.preventDefault();
        if (partId.value === "" || serviceId.value === "") return;

        props.addPartAndService(part, service);
        props.closePopup();
    }

    return (
        <div className='modal'>
            <div className="modal-content" id="id26">
                <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                <div>
                    <div className="login_container">
                        <label><b>Odaberite dio</b></label>
                        <select className="autoservis" onChange={partId.onChange}>
                            <option disabled selected value> -- dio --</option>
                            {allParts.map((part) => {
                                return <option key={part.id} value={part.id}>{part.partName}, {part.price}</option>;
                            })}
                        </select>
                        <br/>
                        <label><b>Odaberite uslugu</b></label>
                        <select className="autoservis" onChange={serviceId.onChange}>
                            <option disabled selected value> -- usluga --</option>
                            {allServices.map((service) => {
                                return <option key={service.id}
                                               value={service.id}>{service.serviceName}, {service.price}</option>;
                            })}
                        </select>
                    </div>
                    <button onClick={onSubmit} className="LoginButton">Odaberi</button>
                </div>

                <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
            </div>
        </div>
    )
}
