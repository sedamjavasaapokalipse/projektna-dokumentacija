import React from 'react';
import '../../App.css';

export default function OrderListItem(props) {

    const vehicleService = props.vehicleService;
    let timestampSplit = vehicleService.timeStarted.split("T");

    return (
        <li>
            <button className="listItem" onClick={(e) => props.toggleUpdateVehicleService(props.vehicleService, e)}>
                <label>{vehicleService.vehicle.brand} </label>
                <label>{vehicleService.vehicle.model} </label> <br/>
                <label> {vehicleService.vehicle.registrationMark} </label> <br/>
                <label> Nalog otvoren: </label>
                <label> {timestampSplit[0]} </label>
                <label> {timestampSplit[1].slice(0, -7)} </label> <br/>
                <label> {vehicleService.order.orderType} </label>
            </button>
        </li>
    )
}