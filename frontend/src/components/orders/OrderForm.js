import '../../App.css';
import React, {useEffect, useState} from 'react';
import {useFormInput} from "../form_utils/FormUtils"
import icon from "../../assets/Layer_1.svg";
import OrderDataService from "../../service/OrderDataService";
import AddPartAndServiceForm from "./AddPartAndServiceForm";

export default function OrderForm(props) {
    const kilometersPassed = useFormInput(() => {
        if (props.vehicleService.kilometersPassed === null) {
            return "";
        }
        return props.vehicleService.kilometersPassed.toString();
    });
    const [newPartsList, setNewPartsList] = useState(null);
    const [servicesList, setServicesList] = useState(null);
    const [showAddPartForm, setShowAddPartForm] = useState(false);
    const mechanic = props.mechanic;


    useEffect(() => {
        console.log(kilometersPassed.value);
        kilometersPassed.value = kilometersPassed.value.replace(/[^0-9]/g, "");
        console.log(kilometersPassed.value);
        kilometersPassed.setValid(parseInt(kilometersPassed.value) <= 3000000);
        kilometersPassed.setValid(kilometersPassed.value.length !== 0)
    }, [kilometersPassed]);


    function toggleAddPart() {
        setShowAddPartForm(show => !show);
    }

    function addPartAndService(part, service) {
        let vs = JSON.parse(JSON.stringify(props.vehicleService.vehicle));
        vs.vehiclePartService = vs.vehiclePartService.concat({
            vehicleService: vs,
            service: service,
            part: part
        });

        OrderDataService
            .updateVehicleService(props.vehicleService.id, vs)
            .then(() => {
                setNewPartsList(list => list.concat([part]));
                setServicesList(list => list.concat([service]));
            })
            .catch(error => console.log(error.toString()));
    }

    function isValid() {
        return kilometersPassed.isValid;
    }

    function setAllChanged() {
        kilometersPassed.setChanged(true);
    }

    function onSubmit(e) {
        e.preventDefault();

        if (!isValid()) {
            setAllChanged();
            return;
        }

        props.vehicleService.kilometersPassed = parseInt(kilometersPassed.value);

        OrderDataService
            .updateVehicleService(props.vehicleService.serviceId, props.vehicleService)
            .then(() => {
                props.closePopup();
            })
            .catch(error => alert(error.toString()));

    }

    return (
        <div className='container'>
            <div className='modal'>
                <div className="modal-content" id="id01">
                    <div className="imgcontainer">
                        <img src={icon} alt="Alat" className="avatar"/>
                    </div>

                    <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                    <form onSubmit={onSubmit}>

                        <div className="login_container">
                            <label><b>Kilometraža</b></label>
                            <input type="text" value={kilometersPassed.value}
                                   className={kilometersPassed.changed && !kilometersPassed.isValid ? "red-border" : ""}
                                   onChange={kilometersPassed.onChange}/>
                            <div className="subTitle"><label><b>Dodani dijelovi i usluge</b></label></div>
                            <ul>
                                {newPartsList &&
                                newPartsList.map(part => {
                                    return <li><label className="left">{part.partName}</label>
                                        <label className="right">{part.price}</label></li>
                                })}
                                {servicesList &&
                                servicesList.map(service => {
                                    return <li><label className="left">{service.serviceName}</label>
                                        <label className="right">{service.price}</label></li>
                                })}
                            </ul>
                            <button className="btnPart" onClick={toggleAddPart}>Dodaj novi dio i uslugu</button>
                            {showAddPartForm &&
                            <AddPartAndServiceForm addPartAndService={addPartAndService}
                                                   closePopup={toggleAddPart}/>}

                        </div>
                        <button type="submit" className="SavePartButton">Uredi nalog</button>
                    </form>

                    <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
                </div>
            </div>
        </div>
    )
}