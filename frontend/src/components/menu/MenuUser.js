import React from 'react';
import '../../App.css';
import {ALL_CAR_SERVICES, LOGOUT, MY_PROFILE, MY_VEHICLES, CREATE_ORDER} from "../../Constants";


export default function MenuUser(props) {

    return (
        <div className={window.innerWidth <= 1024 ? "" : "sideBar"}>
            <ul>
                <li>
                    <button onClick={() => props.handleMenuClick(MY_VEHICLES)}>{MY_VEHICLES}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(ALL_CAR_SERVICES)}>{ALL_CAR_SERVICES}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(MY_PROFILE)}>{MY_PROFILE}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(CREATE_ORDER)}>{CREATE_ORDER}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(LOGOUT)}>{LOGOUT}</button>
                </li>
            </ul>
        </div>
    );
}
