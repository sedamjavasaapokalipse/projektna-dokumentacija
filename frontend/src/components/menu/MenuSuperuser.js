import React from 'react';
import '../../App.css';
import {ALL_CAR_SERVICES, ALL_USERS, LOGOUT} from "../../Constants";


export default function MenuSuperuser(props) {

    return (
        <div className={window.innerWidth <= 1024 ? "" : "sideBar"}>
            <ul>
                <li>
                    <button onClick={() => props.handleMenuClick(ALL_CAR_SERVICES)}>{ALL_CAR_SERVICES}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(ALL_USERS)}>{ALL_USERS}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(LOGOUT)}>{LOGOUT}</button>
                </li>
            </ul>
        </div>
    );
}