import React from 'react';
import '../../App.css';
import {ALL_CAR_PARTS, ALL_MECHANICS, LOGOUT, MY_PROFILE, PRICING_LIST} from "../../Constants";


export default function MenuAdmin(props) {

    return (
        <div className={window.innerWidth <= 1024 ? "" : "sideBar"}>
            <ul>
                <li>
                    <button onClick={() => props.handleMenuClick(ALL_CAR_PARTS)}>{ALL_CAR_PARTS}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(PRICING_LIST)}>{PRICING_LIST}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(ALL_MECHANICS)}>{ALL_MECHANICS}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(MY_PROFILE)}>{MY_PROFILE}</button>
                </li>
                <li>
                    <button onClick={() => props.handleMenuClick(LOGOUT)}>{LOGOUT}</button>
                </li>
            </ul>
        </div>
    );
}
