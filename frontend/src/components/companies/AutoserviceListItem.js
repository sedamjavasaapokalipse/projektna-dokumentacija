import React from 'react';
import '../../App.css';
import image from "../../assets/Auto.svg"


export default function AutoserviceListItem(props) {

    const company = props.company;

    const location = `${company.location.address}, ${company.location.place.placeName}`;

    return (
        <li>
            <button className="btnServ" onClick={props.onClick}>
                <b>{company.companyName}</b>
            </button>

        </li>
    );
}
