import React from 'react'
import AutoserviceListItem from "./AutoserviceListItem";


export default function AutoserviceList(props) {

    const companiesHtmlList = props.companies != null ?
        props.companies.map(c =>
            <AutoserviceListItem key={c.oib} company={c} onClick={() => props.onClick(c)}/>
        ) : (null);

    return (
        <div className="list">
            <h2>Autoservisi</h2>
            {companiesHtmlList}
        </div>


    );
}