import React, {useState, useEffect} from 'react'
import icon from "../../../assets/Layer_1.svg";
import ReviewDataService from "../../../service/ReviewDataService";
import AuthenticationService from "../../../service/AuthenticationService";

export default function AddAutoserviceReviewItem(props) {
    const [userComment, setUserComment] = useState("");
    const [rateNum, setRateNum] = useState("");
    const [badCredentials, setBadCredentials] = useState(false);
//TODO kroz propove trebam dobiti companyName
    function onSubmit(ev) {
        ev.preventDefault();
       const review = {
           rate : rateNum,
           comment : userComment
       };

       console.log(props.company.companyName);
       console.log(AuthenticationService.getLoggedInUserName());

       ReviewDataService
           .createReview(review, props.company.companyName, AuthenticationService.getLoggedInUserName())
           .then(()=> {
               props.updateList(review);
               props.closePopup();
           })
           .catch(reason => alert("add" + reason.toString()));
    }

    function handleUserCommentChange(e) {
        setUserComment(e.target.value);
        setBadCredentials(false);
    }

    function handleRateNumChange(e) {
        setRateNum(e.target.value);
        setBadCredentials(false);
    }

    return(
        <div className='container'>
            <div className='modal'>
                <div className="modal-content" id="id01">
                    <div className="imgcontainer">
                        <img src={icon} alt="Alat" className="avatar"/>
                    </div>

                    <span className="close" onClick={props.closePopup} title="Close Modal">&times;</span>

                    <form onSubmit={onSubmit}>

                        <div className="login_container">
                            <label><b>Vaš komentar </b></label>
                            <input type="text" placeholder="Unesite komentar usluge za ovaj autoservis" name="userComment"
                                   value={userComment}
                                   required onChange={handleUserCommentChange}/>

                            <label><b>Vaša ocjena</b></label>
                            <input type="number" placeholder="Unesite ocjenu od 0 do 5" name="price"
                                   value={rateNum}
                                   required onChange={handleRateNumChange}
                                   step="1"/>

                            {badCredentials &&
                            <label className="bad_credentials">
                                Pogrešno korisničko ime ili lozinka!
                            </label>}
                        </div>
                        <button type="submit" className="SavePartButton">Spremi ovaj osvrt</button>
                    </form>

                    <button type="button" onClick={props.closePopup} className="cancelbtn">Odustani</button>
                </div>
            </div>
        </div>
    );
}