import React, {useState} from 'react'
import UserReview from "./UserReview";


export default function UserReviewsList(props) {

    const reviews = props.reviews != null ?
        props.reviews.map((r) =>
        <li>
            <UserReview key={r.user.id} userReview={r}/>
        </li>) : (null);
    //console.log(props.reviews);
    return(
        <ul>
            {reviews}
        </ul>
    )
}