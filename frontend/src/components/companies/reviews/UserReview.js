import React, {useState} from 'react'
import userAvatar from '../../../assets/iconfinder_7_avatar_2754582.svg'

export default function UserReview(props) {

    const userReview = props.userReview;
    // console.log(props.userReview);

    return(
        <span>
            <img/>
            <div>
                <span>
                    <img src={userAvatar} className="userAvatar"></img>
                    <label className="userName">{userReview.user.username}</label>
                    <label className="userRating">{userReview.rate}</label>
                   {/* <img/>*/}
                </span>
                <textarea disabled className="userComment">{userReview.comment}</textarea>
            </div>
        </span>
    );
}
