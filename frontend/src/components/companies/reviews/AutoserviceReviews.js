import React, {useState, useEffect} from 'react'
import UserReview from "./UserReview";
import AddAutoserviceReviewItem from "./AddAutoserviceReviewItem";
import ReviewDataService from "../../../service/ReviewDataService";
import UserReviewsList from "./UserReviewsList";


export default function AutoserviceReviews(props) {


    const company = props.company;
    const [addReview, setAddReview] = useState(false);
    const [listOfReviews, setListOfReviews] = useState(null);


    function toggleAddReview() {
        setAddReview((addReview) => !addReview);
    }


    // function listReviews() {
    //       ReviewDataService.listAll(company.companyName)
    //           .then(r =>
    //           {
    //              let reviews = r.data.map(
    //               (item) => <li key={item.id}>
    //                   <div className={"partFont"}>
    //                       <img/>
    //                       <label>{item.user.username}</label>
    //                       <br/>
    //                       <label>{item.comment}</label>
    //
    //                   </div>
    //               </li>);
    //               setListOfReviews(reviews);
    //               // console.log(listOfReviews);
    //           }
    //           )
    //           .catch(reason => alert(reason.toString()));
    // }

    function listReviews() {
        ReviewDataService.listAll(company.companyName)
            .then(r => {
                    const data = r.data;
                    const listItems = data.map((d) => <li key={d.comment}>{d.comment}</li>);
                    setListOfReviews(listItems);
                }
            )
            .catch(reason => alert(reason.toString()));
    }

    useEffect(() => listReviews(), []);


    return (
        <div className="reviewContainer">

            <label>{company.companyName}</label>
            <button>Order Service</button>

            <br/>

            <label>Reviews</label>
            <button onClick={toggleAddReview}>+Add Review</button>
            <br/>

            <span className={"partFont"}>

            </span>


            {
                addReview &&
                <AddAutoserviceReviewItem
                    company={company}
                    updateList={listReviews}
                    closePopup={toggleAddReview}/>
            }
            {listOfReviews != null ?
                <div>
                    {listOfReviews}
                </div> : <p/>
            }
            {/*{listOfReviews != null &&*/}
            {/*    <UserReviewsList reviews={listOfReviews}/>}*/}
               {/* <UserReviewsList reviews={
                [
                    {
                        user: {
                            username: "lubi"
                        },
                        rate: "4",
                        comment: "lip komentar"

                    },
                    {
                        user: {
                            username: "stipe"
                        },
                        rate: "5",
                        comment: "bolji komentar"
                    }
                ]}/>*/}


        </div>
    );

}
