import React, {useEffect, useState} from 'react'
import AutoserviceList from "./AutoserviceList";
import AutoservisDataService from "../../service/AutoservisDataService";
import AutoserviceReviews from "./reviews/AutoserviceReviews";

export default function AllAutoservicesContainer() {

    const [company, setCompany] = useState(null);
    const [companies, setCompanies] = useState(null);

    function handleClick(c) {
        setCompany(c);
    }

    useEffect(() => {
        AutoservisDataService
            .getAutoservices()
            .then(response => {
                setCompanies(response.data);
            })
            .catch(reason => alert("container" + reason.toString()))
    }, []);

    return (
        <div>
            {companies !==null &&
            <AutoserviceList onClick={handleClick} companies={
                companies
            }/>}
            {
              company !== null &&  <AutoserviceReviews company={company}/>
            }


        </div>

    );


}