import React from 'react'
import '../App.css'

export default function Footer() {

    return (
        <footer className='footer-container'>
            <div className="container">
                <p className="rights">©2020 MojAutoServis. All Rights Reserved.</p>
            </div>
        </footer>
    );
}
