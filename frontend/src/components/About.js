import React from 'react'
import '../App.css'
import Calendar from "../assets/CalendarIcon.svg"
import Computer from "../assets/ComputerIcon.svg"
import Tool from "../assets/Layer_1.svg"

export default function About() {

    return (
        <div className="black-container">
            <div className="container">
                <ul>
                    <li><img src={Calendar} alt="Calendar-icon"/>
                        <p>Dostupni bilo koji dan, te u bilo koje vrijeme dana. Osigurajte pregled svog vozila kada vama
                            odgovara.</p></li>
                    <li><img src={Computer} alt="Computer-icon"/>
                        <p>Najednostavniji način dobivanja servisa za vaš automobil. Iz udobnosti vašeg doma, ili ako
                            ste u pokretu.</p></li>
                    <li><img src={Tool} alt="Calendar-icon"/>
                        <p>Najbolji serviseri u vašemu gradu na jednome mjestu. Osigurajte svome limenom ljubimcu
                            najbolju uslugu.</p></li>
                </ul>
            </div>
        </div>
    );
}
