import React, {useState} from 'react'
import '../App.css'
import logo from '../assets/Logo.svg'
import ham from '../assets/ham.svg'
import exit from '../assets/exit.svg'
import Login from "./users/Login";
import Register from "./users/Register";



export default function Header(props) {
    const [showLogin, setShowLogin] = useState(false);
    const [showRegister, setShowRegister] = useState(false);
    const [showSidebar, setShowSidebar] = useState(false);


    function toggleLogin() {
        setShowLogin(show => !show)
    }

    function toggleRegister() {
        setShowRegister(show => !show)
    }

    function toggleBox() {
        setShowSidebar(show => !show)
    }
    
    function onImageClick() {
        if (props.loggedIn){
            props.mainPage();
        }
    }

    return (
        <div className="container">
            <header>
                <img onClick={onImageClick} src={logo} alt="logo" className="logo"/>
                <nav>
                    <button className="hide-desktop" onClick={toggleBox}>
                        <img src={ham} alt="toggle menu" className="menu" id="menu"/>
                    </button>
                    <div className={showSidebar && window.innerWidth<=1024 ? "modal" : ""} onClick={toggleBox}>
                        <ul className={showSidebar ? "" : "show-desktop hide-mobile"} id="nav">


                            <li> {window.innerWidth <= 1024 ? props.Menu() : <span/>}</li>

                            <li id="exit" className="hide-desktop">
                                <button className="exit-btn" onClick={toggleBox}><img src={exit} alt="exit menu"/>
                                </button>
                            </li>
                            {!props.loggedIn &&
                            <span>
                                <li>
                                    <button onClick={toggleLogin}>Prijava</button>
                                </li>
                                <li>
                                    <button onClick={toggleRegister}>Registracija</button>
                                </li>
                            </span>
                            }
                        </ul>
                    </div>
                </nav>
            </header>
            {

            }
            {showLogin &&
            <Login
                closePopup={toggleLogin}
                onLogin={props.onLogin}
            />}
            {showRegister &&
            <Register
                closePopup={toggleRegister}
                onLogin={props.onLogin}
            />}
        </div>
    );
}
