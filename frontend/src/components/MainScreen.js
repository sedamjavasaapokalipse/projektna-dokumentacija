import React from 'react'
import '../App.css'
import carLogo from "../assets/Auto.svg";
import arrow from "../assets/Strelica.svg"
import AuthenticationService from "../service/AuthenticationService";

export default function MainScreen(props) {

    return (
        <div className="container">
            {!props.loggedIn ? <img src={carLogo} alt="auto graphic" className="carLogo"/> : <div/>}
            {!props.loggedIn ?
                <h1>Moj Auto Servis</h1>
                : <div/>}
            {props.loggedIn ?
                <p className="subhead">
                    {"Korisničko ime: " + AuthenticationService.getLoggedInUserName()}
                </p> :
                <p className="subhead">
                    Sve usluge za vaš automobil koje će te ikad trebati na jednome mjestu. Tisuće zadovoljnih korisnika,
                    te stotine registriranih auto servisa.
                </p>
            }
            {!props.loggedIn ? <img src={arrow} alt="go down" className="scroll hide-mobile show-desktop"/> :
                <div/>}
        </div>
    );
}
