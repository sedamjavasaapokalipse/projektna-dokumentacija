export const API_URL = 'http://localhost:8080';
export const LOCAL_ATTRIBUTE_USERNAME = 'authUsername';

export const MAIN_SCREEN = 'MainScreen';
export const MY_VEHICLES = 'Moja Vozila';
export const ALL_CAR_SERVICES = 'Autoservisi';
export const MY_PROFILE = 'Moj Profil';
export const ALL_CAR_PARTS = 'Autodijelovi';
export const PRICING_LIST = 'Cjenik';
export const ALL_MECHANICS = 'Automehaničari';
export const ALL_USERS = 'Korisnici';
export const CREATE_ORDER = 'Otvori nalog';
export const FILL_ORDER = "Popuni nalog";
export const LOGOUT = 'Odjava';

export const REQUIRED_MESSAGE = "Ovo polje je obavezno";
export const OIB_UNAVAILABLE = "OIB već postoji";
export const WRONG_OIB_MESSAGE = "OIB mora sadržavati 11 znamenki";
export const USERNAME_UNAVAILABLE = "Korisničko ime već postoji";
export const COMPANY_NAME_UNAVAILABLE = "Naziv autoservisa već postoji";
export const WRONG_USERNAME = "Korisničko ime ne smije sadržavati znak '@'";
export const WRONG_COMPANY_NAME = "Naziv autoservisa ne smije sadržavati znak '@'";
export const WRONG_POSTAL_CODE = "Poštanski broj mora biti peteroznamenkasti broj";
export const EMAIL_UNAVAILABLE = "Email već postoji";
export const WRONG_EMAIL_MESSAGE = "Pogrešan email";
export const PASSWORD_TOO_SHORT = "Lozinka mora sadržavati najmanje 6 znakova";
export const PASSWORD_MISMATCH = "Lozinke se ne podudaraju";
export const WRONG_OLD_PASSWORD = "Pogrešna lozinka";
export const REGISTRATION_MARK_UNAVAILABLE = "Registarska oznaka već postoji";

export const ILLEGAL_NUMBER_MESSAGE = "Vrijednost mora biti pozitivan broj";

export const ILLEGAL_REGISTRATION_NUMBER = "Nevažeća registarska oznaka";
