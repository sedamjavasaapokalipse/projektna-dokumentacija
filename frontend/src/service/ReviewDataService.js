import axios from 'axios'
import {API_URL} from '../Constants'

class ReviewDataService {

    createReview(review, companyName, username) {
        return axios.post(`${API_URL}/reviews?companyName=${companyName}&username=${username}`, review);
    }

    listAll(companyName) {
        return axios.get(`${API_URL}/reviews/${companyName}`);
    }
}

export default new ReviewDataService()