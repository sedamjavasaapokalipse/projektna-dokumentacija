import axios from 'axios'
import { API_URL} from '../Constants'

class PartDataService {

    createPart(part, companyName) {
        return axios.post(`${API_URL}/parts/create/${companyName}`, part);
    }

    updatePart(id, part) {
        return axios.post(`${API_URL}/parts/update/${id}`, part);
    }

    listAllByCompanyName(companyName) {
        return axios.get(`${API_URL}/parts/by-company-name/${companyName}`);
    }

    listAllByMechanic(mechanicName) {
        return axios.get(`${API_URL}/parts/by-mechanic/${mechanicName}`);
    }

    deletePart(id) {
        return axios.delete(`${API_URL}/parts/${id}`);
    }

    getPart(id) {
        return axios.get(`${API_URL}/parts/${id}`);
    }
}

export default new PartDataService()