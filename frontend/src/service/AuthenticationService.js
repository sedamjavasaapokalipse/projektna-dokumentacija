import axios from 'axios'
import { API_URL, LOCAL_ATTRIBUTE_USERNAME } from '../Constants'

class AuthenticationService {

    executeJwtAuthenticationService(usernameOrEmail, password) {
        return axios.post(`${API_URL}/authenticate`, {
            username: usernameOrEmail,
            password
        })
    }

    registerSuccessfulLoginForJwt(username, token) {
        localStorage.setItem(LOCAL_ATTRIBUTE_USERNAME, username);
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }
    
    logout() {
        localStorage.removeItem(LOCAL_ATTRIBUTE_USERNAME);
    }

    isUserLoggedIn() {
        let user = localStorage.getItem(LOCAL_ATTRIBUTE_USERNAME);
        return user !== null;
    }

    getLoggedInUserName() {
        let user = localStorage.getItem(LOCAL_ATTRIBUTE_USERNAME);
        if (user === null) return '';
        return user
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()