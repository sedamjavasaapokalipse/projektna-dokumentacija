import axios from 'axios'
import {API_URL} from '../Constants'

class HuoDataService {

    existsInHuoRegister(registrationMark){
        return axios.get(`${API_URL}/huo/${registrationMark}`);
    }

}

export default new HuoDataService()