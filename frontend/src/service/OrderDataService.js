import axios from 'axios'
import { API_URL} from '../Constants'

class OrderDataService {

    retrieveVehicleServicesByMechanic(mechanicName) {
        return axios.get(`${API_URL}/vehicle-service/by_mechanic/${mechanicName}`)
    }

    retrieveVehicleServicesByUsername(username) {
        return axios.get(`${API_URL}/vehicle-service/by_user/${username}`)
    }

    createVehicleService(vehicleService) {
        return axios.post(`${API_URL}/vehicle-service`, vehicleService);
    }

    updateVehicleService(id, vehicleService) {
        return axios.patch(`${API_URL}/vehicle-service/${id}`, vehicleService);
    }
}

export default new OrderDataService()