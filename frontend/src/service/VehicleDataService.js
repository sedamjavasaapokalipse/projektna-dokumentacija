import axios from 'axios'
import {API_URL} from "../Constants"

class VehicleDataService {
    createVehicle(vehicle, username) {
        return axios.post(`${API_URL}/vehicles/${username}`, vehicle);
    }

    updateVehicle(vehicle, id) {
        return axios.post(`${API_URL}/vehicles/update/${id}`, vehicle);
    }

    getVehicle(registrationMark) {
        return axios.get(`${API_URL}/vehicles/${registrationMark}`);
    }

    getVehicles(username) {
        return axios.get(`${API_URL}/vehicles?username=${username}`);
    }

    deleteVehicle(registrationMark) {
        return axios.delete(`${API_URL}/vehicles/${registrationMark}`);
    }

    isRegistrationMarkAvailable(registrationMark) {
        return axios.get(`${API_URL}/vehicles/available?registrationMark=${registrationMark}`)
    }
}

export default new VehicleDataService()