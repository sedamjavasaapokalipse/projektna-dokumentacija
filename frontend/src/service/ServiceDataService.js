import axios from 'axios'
import {API_URL} from "../Constants"

class ServiceDataService {

    listServices(companyName){
        return axios.get(`${API_URL}/services/${companyName}`);
    }

    createService(companyName, service){
        return axios.post(`${API_URL}/services/create/${companyName}`, service);
    }

    updateService(id, service){
        return axios.post(`${API_URL}/services/update/${id}`, service);
    }

    deleteService(id){
        return axios.delete(`${API_URL}/services/${id}`);
    }

    getService(id){
        return axios.get(`${API_URL}/services/${id}`);
    }

    listAllByMechanic(mechanicName) {
        return axios.get(`${API_URL}/services/by-mechanic/${mechanicName}`);
    }

}

export default new ServiceDataService()
