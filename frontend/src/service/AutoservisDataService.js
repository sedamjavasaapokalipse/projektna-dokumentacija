import axios from 'axios'
import { API_URL} from '../Constants'

class AutoservisDataService {

    createAutoservis(autoservis, user) {
        return axios.post(`${API_URL}/autoservisi`, {autoservis: autoservis, user: user});
    }

    getAutoservices(){
        return axios.get(`${API_URL}/autoservisi`);
    }

    updateAutoservis(user) {
        return axios.patch(`${API_URL}/autoservisi`, user);
    }

    getCompany(companyName) {
        return axios.get(`${API_URL}/autoservisi/${companyName}`)
    }

    getCompanyById(id) {
        return axios.get(`${API_URL}/autoservisi/by_id/${id}`)
    }

    retrieveAutoservisi() {
        return axios.get(`${API_URL}/autoservisi`)
    }

    getMechanics(companyName) {
        return axios.get(`${API_URL}/autoservisi/mechanics/${companyName}`)
    }

    getReviews(autoservis){
        return axios.get(`${API_URL}/autoservisi/`)
    }
}

export default new AutoservisDataService()