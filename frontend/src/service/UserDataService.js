import axios from 'axios'
import {API_URL} from '../Constants'

class UserDataService {

    createUser(user) {
        return axios.post(`${API_URL}/users`, user);
    }

    createMechanic(companyName, user) {
        return axios.post(`${API_URL}/users/${companyName}`, user);
    }

    removeMechanic(companyName, username) {
        return axios.patch(`${API_URL}/users/remove/${username}`)
    }

    updateUser(user) {
        return axios.patch(`${API_URL}/users`, user);
    }

    updateUserPassword(id, user) {
        return axios.patch(`${API_URL}/users/${id}`, user);
    }

    checkPassword(username, user){
        return axios.patch(`${API_URL}/users/check/${username}`, user);
    }

    deleteUser(username) {
        return axios.delete(`${API_URL}/users/${username}`);
    }

    getUser(username) {
        return axios.get(`${API_URL}/users/${username}`);
    }

    getRole(username) {
        return axios.get(`${API_URL}/userRoles/${username}`);
    }

    getUserByEmail(email) {
        return axios.get(`${API_URL}/users/by_email?email=${email}`, email)
    }

    isUsernameAvailable(username) {
        return axios.get(`${API_URL}/users/availableUsername?username=${username}`)
    }

    isEmailAvailable(email) {
        return axios.get(`${API_URL}/users/availableEmail?email=${email}`)
    }

    isOibAvailable(oib) {
        return axios.get(`${API_URL}/users/availableOib?oib=${oib}`)
    }

}

export default new UserDataService()
