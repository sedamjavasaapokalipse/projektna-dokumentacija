import React, {useEffect, useState} from 'react';
import './App.css';
import Header from './components/Header'
import MainScreen from './components/MainScreen'
import About from "./components/About";
import Reviews from "./components/Reviews";
import Footer from "./components/Footer";
import AuthenticationService from "./service/AuthenticationService";
import UserDataService from "./service/UserDataService";
import MenuMechanic from "./components/menu/MenuMechanic";
import MenuAdmin from "./components/menu/MenuAdmin";
import MenuUser from "./components/menu/MenuUser";
import MenuSuperuser from "./components/menu/MenuSuperuser";
import ShowPersonalData from "./components/users/ShowPersonalData";
import Order from "./components/orders/Order";
import OrderList from "./components/orders/OrderList";
import {
    ALL_CAR_PARTS,
    ALL_CAR_SERVICES,
    ALL_MECHANICS,
    ALL_USERS,
    CREATE_ORDER, FILL_ORDER,

    LOGOUT,
    MAIN_SCREEN,
    MY_PROFILE,
    MY_VEHICLES,
    PRICING_LIST
} from "./Constants";
import PartsList from "./components/parts/PartsList";
import ServicesList from "./components/services/ServicesList"
import VehiclesList from "./components/vehicles/VehiclesList";
import MechanicsList from "./components/mechanics/MechanicsList";
import AllAutoservicesContainer from "./components/companies/AllAutoservicesContainer";
import AutoservisDataService from "./service/AutoservisDataService";


export default function App() {
    const [loggedIn, setLoggedIn] = useState(AuthenticationService.isUserLoggedIn());
    const [role, setRole] = useState(null);
    const [user, setUser] = useState(null);
    const [company, setCompany] = useState(null);

    const [mainContainerContent, setMainContainerContent] = useState(MAIN_SCREEN);

    useEffect(() => {
        document.title = "MojAutoServis";
    }, []);

    useEffect(() => {
        if (loggedIn && role === null) {
            updateLocalRole();
        }
        if (loggedIn && user === null) {
            UserDataService
                .getUser(AuthenticationService.getLoggedInUserName())
                .then(response => setUser(response.data));
        }
    }, [loggedIn]);

    function onLogin() {
        setLoggedIn(true);
    }

    function logout() {
        AuthenticationService.logout();
        setLoggedIn(false);
        setRole(null);
        setUser(null);
        setMainContainerContent(MAIN_SCREEN)
    }

    function updateLocalRole(role) {
        if (role) {
            console.log("updated role " + role);
            setRole(role);
            return;
        }
        console.log("updating local role " + AuthenticationService.getLoggedInUserName());
        UserDataService
            .getRole(AuthenticationService.getLoggedInUserName())
            .then(response => {
                let role = response.data;
                setRole(role);
                if (role === "ADMIN") {
                    AutoservisDataService
                        .getCompany(AuthenticationService.getLoggedInUserName())
                        .then(r => setCompany(r.data))
                        .catch(error => alert(error.toString()));
                }
                console.log("updated local role");
            })
            .catch(error => console.log(error.toString()));
    }

    function handleMenuClick(key) {
        if (key === LOGOUT) logout();
        else setMainContainerContent(key);
    }

    function Menu() {
        if (!loggedIn || role === null) {
            return <p/>;
        }

        switch (role.toString()) {
            case "SUPERUSER" :
                return <MenuSuperuser handleMenuClick={handleMenuClick}/>;
            case "ADMIN" :
                return <MenuAdmin handleMenuClick={handleMenuClick}/>;
            case "AUTO_MECHANIC" :
                return <MenuMechanic handleMenuClick={handleMenuClick}/>;
            case "REGISTERED_USER" :
                return <MenuUser handleMenuClick={handleMenuClick}/>;
        }
    }

    function MainContainer() {
        if (!loggedIn || role === null || mainContainerContent === MAIN_SCREEN) {
            return <MainScreen loggedIn={loggedIn}/>;
        }

        switch (mainContainerContent) {
            case MY_VEHICLES:
                return <VehiclesList/>;
            case ALL_CAR_SERVICES:
                return <AllAutoservicesContainer/>;
            case MY_PROFILE:
                return <ShowPersonalData user={user} role={role} company={company}
                                         setUser={setUser} setCompany={setCompany}
                                         goBack={() => setMainContainerContent(MAIN_SCREEN)}
                                         logout={logout}/>;
            case ALL_CAR_PARTS:
                return <PartsList user={user} role={role}/>;
            case PRICING_LIST:
                return <ServicesList user={user} role={role}/>;
            case ALL_MECHANICS:
                return <MechanicsList/>;
            case ALL_USERS:
                break;
            case CREATE_ORDER:
                return <Order/>;
            case FILL_ORDER:
                return <OrderList/>
        }

        return <MainScreen loggedIn={loggedIn}/>;
    }

    return (
        <div className="App">
            <Header
                onLogin={onLogin}
                loggedIn={loggedIn}
                logout={logout}
                Menu={Menu}
                mainPage={() => setMainContainerContent(MAIN_SCREEN)}
            />

            {window.innerWidth <= 1024 ? <span/> : <Menu/>}
            <MainContainer/>
            {loggedIn ? <div/>
                : <div><About/><Reviews/></div>
            }
            <Footer/>
        </div>
    );
}
